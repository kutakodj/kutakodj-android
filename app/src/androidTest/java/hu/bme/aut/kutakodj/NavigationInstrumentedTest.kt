package hu.bme.aut.kutakodj

import android.Manifest
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import com.google.common.truth.Truth.assertThat
import com.schibsted.spain.barista.assertion.BaristaImageViewAssertions.assertHasDrawable
import com.schibsted.spain.barista.assertion.BaristaListAssertions.assertListNotEmpty
import com.schibsted.spain.barista.assertion.BaristaVisibilityAssertions.assertDisplayed
import com.schibsted.spain.barista.interaction.BaristaClickInteractions.clickBack
import com.schibsted.spain.barista.interaction.BaristaClickInteractions.clickOn
import com.schibsted.spain.barista.interaction.BaristaListInteractions.clickListItem
import com.schibsted.spain.barista.interaction.BaristaListInteractions.scrollListToPosition
import com.schibsted.spain.barista.interaction.BaristaScrollInteractions.scrollTo
import com.schibsted.spain.barista.interaction.BaristaSleepInteractions
import com.schibsted.spain.barista.interaction.BaristaViewPagerInteractions.swipeViewPagerBack
import com.schibsted.spain.barista.interaction.BaristaViewPagerInteractions.swipeViewPagerForward
import com.schibsted.spain.barista.rule.cleardata.ClearPreferencesRule
import hu.bme.aut.kutakodj.view.activity.MainActivity
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class NavigationInstrumentedTest {

    val GLOBAL_TIMEOUT = 2000L

    @Rule
    @JvmField
    var baristaRule: ActivityTestRule<MainActivity> = ActivityTestRule(
        MainActivity::class.java, true, false
    )

    @Rule
    @JvmField
    val clearPreferencesRule: ClearPreferencesRule = ClearPreferencesRule()


    // https://stackoverflow.com/a/45320863
    // espresso cant interact with the system permission dialog yay...
    @get:Rule
    var permissionRule: GrantPermissionRule = GrantPermissionRule.grant(Manifest.permission.CAMERA)

    @Before
    fun setUp() {
        baristaRule.launchActivity(null)
    }

    @After
    fun close() {
        baristaRule.finishActivity()
    }

    @Test
    fun checkGetActivity() {
        assertThat(baristaRule.activity)
            .isNotNull()
    }

    @Test
    fun checkNavigateToMain() {
        // We are on the welcome page
        assertDisplayed(R.id.tv_mainTitle, "KUTAKODJ")
        // We click the well
        clickOn(R.id.civ_mainStartLogo)
        // Navigate to the end of the info panels
        assertDisplayed(R.id.civ_infoAnim)
        swipeViewPagerForward()
        assertDisplayed(R.id.civ_infoAnim)
        swipeViewPagerForward()
        assertDisplayed(R.id.civ_infoAnim)
        swipeViewPagerForward()

        //Check if we arrived to the MainPage/CollectionPage
        assertDisplayed(R.id.view_animator_collection)
    }

    @Test
    fun checkBackPressFromMainToWelcome() {
        navigateToMain()
        clickBack()
        assertDisplayed(R.id.tv_mainTitle, "KUTAKODJ")
    }

    @Test
    fun checkNavigateToAbout() {
        navigateToMain()
        // click on info
        clickOn(R.id.btn_info)
        assertDisplayed(R.id.civ_about)
        // click on about
        clickOn(R.id.civ_about)
        BaristaSleepInteractions.sleep(GLOBAL_TIMEOUT)
        assertDisplayed(R.id.tv_about_content)
    }

    @Test
    fun checkBackPressFromAboutToMain() {
        navigateToAboutFromMain()
        clickBack()
        assertDisplayed(R.id.civ_about)
        clickBack()
        assertDisplayed(R.id.view_animator_collection)
    }

    @Test
    fun checkNavigateToFAQ() {
        navigateToMain()
        // click on info
        clickOn(R.id.btn_info)
        assertDisplayed(R.id.civ_gyik)
        // click on gyik
        clickOn(R.id.civ_gyik)
        assertDisplayed(R.id.civ_infoAnim)
    }

    // We just test whether we go back to the info choice
    @Test
    fun checkBackPressFromFAQ() {
        navigateToFAQ()
        clickBack()
        assertDisplayed(R.id.civ_gyik)
    }

    @Test
    fun checkNavigateFromFAQToMain() {
        navigateToFAQ()
        assertDisplayed(R.id.civ_infoAnim)
        swipeViewPagerForward()
        assertDisplayed(R.id.civ_infoAnim)
        swipeViewPagerForward()
        assertDisplayed(R.id.civ_infoAnim)
        swipeViewPagerForward()
        assertDisplayed(R.id.view_animator_collection)
    }

    @Test
    fun checkBackPressAfterFAQ() {
        navigateToFAQ()
        navigateThroughFAQ()
        clickBack()
        assertDisplayed(R.id.tv_mainTitle, "KUTAKODJ")
    }

    @Test
    fun checkNavigateFromMainToQuiz() {
        navigateToMain()
        swipeViewPagerBack()
        assertDisplayed(R.id.tv_quiz)
        assertDisplayed(R.id.recyclerview_quiz)
    }

    @Test
    fun checkNavigateFromReadToQuestion() {
        navigateToQuiz()
        scrollTo(R.id.civ_action)
        clickOn(R.id.civ_action)
        // we mock the camera input read data
        baristaRule.runOnUiThread {
            baristaRule.activity.onReadDataAcquired("001")
        }
        BaristaSleepInteractions.sleep(GLOBAL_TIMEOUT)
        assertDisplayed(R.id.tv_station_name)
        assertDisplayed(R.id.tv_question)
        assertDisplayed(R.id.recyclerview_answers)
        assertListNotEmpty(R.id.recyclerview_answers)
    }

    // todo quiz branch nav

    @Test
    fun checkBackPressFromQuestion() {
        // We will get the first question
        //navigateToQuestion()
        //clickListItem(R.id.recyclerview_answers, 2)
        //BaristaSleepInteractions.sleep(GLOBAL_TIMEOUT)
    }

    @Test
    fun checkNavigateFromMainToWell() {
        navigateToMain()
        swipeViewPagerForward()
        assertDisplayed(R.id.tv_well)
        assertDisplayed(R.id.recyclerview_well)
        assertListNotEmpty(R.id.recyclerview_well)
    }

    @Test
    fun checkNavigateFromWellToUnknownItem() {
        navigateToWell()
        scrollTo(R.id.recyclerview_well)
        scrollListToPosition(R.id.recyclerview_well, 1)
        clickListItem(R.id.recyclerview_well, 1)
        assertDisplayed(R.id.civ_object_picture)
        assertHasDrawable(R.id.civ_object_picture, R.drawable.question_mark)
        assertDisplayed(R.id.tv_object_name, "????")
        assertDisplayed(R.id.tv_object_description, "????")
    }

    @Test
    fun checkBackPressAfterUnknownItem() {
        navigateToWell()
        scrollTo(R.id.recyclerview_well)
        scrollListToPosition(R.id.recyclerview_well, 1)
        clickListItem(R.id.recyclerview_well, 1)
        clickBack()
        assertDisplayed(R.id.tv_well)
        assertDisplayed(R.id.recyclerview_well)
        assertListNotEmpty(R.id.recyclerview_well)
    }

    @Test
    fun checkBackButtonAfterUnknownItem() {
        navigateToWell()
        scrollTo(R.id.recyclerview_well)
        scrollListToPosition(R.id.recyclerview_well, 1)
        clickListItem(R.id.recyclerview_well, 1)
        clickOn(R.id.frame_back)
        assertDisplayed(R.id.tv_well)
        assertDisplayed(R.id.recyclerview_well)
        assertListNotEmpty(R.id.recyclerview_well)
    }

    @Test
    fun checkNavigateFromWellToRead() {
        navigateToWell()
        scrollTo(R.id.civ_action_well)
        clickOn(R.id.civ_action_well)
        assertDisplayed(R.id.view_animator_read)
    }

    @Test
    fun checkBackPressFromRead() {
        navigateToWell()
        clickOn(R.id.civ_action_well)
        clickBack()
        assertDisplayed(R.id.tv_well)
        assertDisplayed(R.id.recyclerview_well)
        assertListNotEmpty(R.id.recyclerview_well)
    }

    @Test
    fun checkNavigateToReadObject() {
        navigateToWell()
        scrollTo(R.id.civ_action_well)
        clickOn(R.id.civ_action_well)
        // we kinda mock the camera reading
        baristaRule.runOnUiThread {
            baristaRule.activity.onReadDataAcquired("012")
        }
        BaristaSleepInteractions.sleep(GLOBAL_TIMEOUT)
        assertDisplayed(R.id.tv_object_name)
    }

    private fun navigateToMain() {
        clickOn(R.id.civ_mainStartLogo)
        navigateThroughFAQ()
    }

    private fun navigateToAboutFromMain() {
        navigateToMain()
        clickOn(R.id.btn_info)
        clickOn(R.id.civ_about)
    }

    private fun navigateToFAQ() {
        navigateToMain()
        clickOn(R.id.btn_info)
        clickOn(R.id.civ_gyik)
    }

    private fun navigateThroughFAQ() {
        for (i in 0 until 3) {
            swipeViewPagerForward()
        }
    }

    private fun navigateToWell() {
        navigateToMain()
        swipeViewPagerForward()
    }

    private fun navigateToQuiz() {
        navigateToMain()
        swipeViewPagerBack()
    }

    private fun navigateToQuestion(id: String = "001") {
        navigateToQuiz()
        scrollTo(R.id.civ_action)
        clickOn(R.id.civ_action)
        // we mock the camera input read data
        baristaRule.runOnUiThread {
            baristaRule.activity.onReadDataAcquired(id)
        }
        BaristaSleepInteractions.sleep(GLOBAL_TIMEOUT)
    }
}