package hu.bme.aut.kutakodj

import hu.bme.aut.kutakodj.util.CodeGenerator
import io.kotest.core.spec.style.FeatureSpec
import io.kotest.matchers.shouldBe

class CodeGeneratorTest : FeatureSpec({
    lateinit var codeGenerator: CodeGenerator

    feature("a code generator") {
        beforeTest {
            codeGenerator = CodeGenerator()
        }
        scenario("generates a code of 6 length") {
            val code = codeGenerator.generateCodeForPlayer()
            code.length shouldBe 6
        }
        scenario("generates a code with an even center") {
            val code = codeGenerator.generateCodeForPlayer()
            val center = code.substring(2..3)

            center.toInt() % 2 shouldBe 0
        }

        scenario("generates a code with the first and last two digits - 2 rem 7") {
            val code = codeGenerator.generateCodeForPlayer()
            val sides = code.substring(0..1) + code.substring(4..5)

            (sides.toInt() - 2) % 7 shouldBe 0
        }
    }
})