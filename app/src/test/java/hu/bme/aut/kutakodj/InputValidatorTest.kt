package hu.bme.aut.kutakodj

import hu.bme.aut.kutakodj.util.inputvalidator.InputValidator
import hu.bme.aut.kutakodj.util.inputvalidator.MobileInputValidator
import hu.bme.aut.kutakodj.util.inputvalidator.PuzzleInputValidator
import io.kotest.core.spec.style.FeatureSpec
import io.kotest.matchers.shouldBe

class InputValidatorTest : FeatureSpec({

    lateinit var inputValidator: InputValidator

    feature("a mobile input validator") {
        beforeTest {
            inputValidator = MobileInputValidator()
        }
        scenario("provides an id when the input is valid") {
            val res = inputValidator.validateInput("KUTAKODJ:001:001")
            res shouldBe "001"
        }
        scenario("provides an empty string when the input is not valid") {
            val res1 = inputValidator.validateInput("This is not a valid input")
            val res2 = inputValidator.validateInput("This:is:not:a:valid:input")
            val res3 = inputValidator.validateInput("This:Is:001")
            res1 shouldBe ""
            res2 shouldBe ""
            res3 shouldBe ""
        }
        scenario("provides an empty string when the input is a puzzle read") {
            val res = inputValidator.validateInput("KUTAKODJ:1234:OK")
            res shouldBe ""
        }
    }

    feature("a puzzle input validator") {
        beforeTest {
            inputValidator = PuzzleInputValidator()
        }
        scenario("provides a playercode when the input is valid with OK") {
            val res = inputValidator.validateInput("KUTAKODJ:1234:OK")
            res shouldBe "1234"
        }
        scenario("provides an empty string when the input is not valid") {
            val res1 = inputValidator.validateInput("This is not a valid input")
            val res2 = inputValidator.validateInput("This:is:not:a:valid:input")
            val res3 = inputValidator.validateInput("This:Is:001")
            res1 shouldBe ""
            res2 shouldBe ""
            res3 shouldBe ""
        }
        scenario("provides an empty string when the input suggest the puzzle wasnt solved") {
            val res = inputValidator.validateInput("KUTAKODJ:PLAYERCODE:NOTOK")
            res shouldBe ""
        }
        scenario("provides an empty string when the input is a read object") {
            val res = inputValidator.validateInput("KUTAKODJ:001:001")
            res shouldBe ""
        }
    }
})