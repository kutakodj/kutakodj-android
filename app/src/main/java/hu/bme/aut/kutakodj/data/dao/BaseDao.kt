package hu.bme.aut.kutakodj.data.dao

import androidx.room.* // ktlint-disable no-wildcard-imports

@Dao
interface BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg obj: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(obj: T): Long

    @Delete
    suspend fun delete(obj: T)

    @Update
    suspend fun update(obj: T)
}
