package hu.bme.aut.kutakodj.view.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.events.navigation.StuffReadEvent
import hu.bme.aut.kutakodj.events.navigation.TaskRequestedEvent
import hu.bme.aut.kutakodj.logic.enums.ReadableType
import hu.bme.aut.kutakodj.logic.viewmodels.QuizViewModel
import hu.bme.aut.kutakodj.navigation.getNavOwner
import hu.bme.aut.kutakodj.view.adapter.QuizListAdapter
import kotlinx.android.synthetic.main.fragment_quiz.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.koin.androidx.viewmodel.ext.android.getViewModel

class QuizFragment : Fragment(R.layout.fragment_quiz) {
    private lateinit var quizViewModel: QuizViewModel
    private var adapter: QuizListAdapter? = null

    private object Animator {
        const val COLLECTED = 0
        const val COLLECTING = 1
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        quizViewModel = getViewModel()
        initData()
    }

    private fun initData() {
        quizViewModel.loadRoomByName("Quiz")

        if (!quizViewModel.quizStuffs.hasActiveObservers()) {
            quizViewModel.quizStuffs.observe(viewLifecycleOwner, Observer { stuffs ->
                stuffs?.let { list ->
                    if (list.isNotEmpty()) {
                        if (list.all { it.stuff.isCollected }) {
                            view_animator_quiz.displayedChild = Animator.COLLECTED
                        } else {
                            view_animator_quiz.displayedChild = Animator.COLLECTING
                            if (adapter == null) {
                                adapter = QuizListAdapter(requireActivity())
                            }
                            initRecyclerView()
                            adapter?.let { adapter ->
                                adapter.setItems(list.filter { !it.stuff.isCollected })
                            }
                        }
                    }
                }
            })
        }
    }

    private fun initRecyclerView() {
        recyclerview_quiz.adapter = adapter
        recyclerview_quiz.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
    }

    override fun onResume() {
        super.onResume()
        civ_action.setOnClickListener {
            requireActivity()
                .getNavOwner()
                ?.replaceFragment(ReadFragment().apply {
                    arguments = Bundle().apply {
                        this.putInt(ReadFragment.READABLE_TYPE_KEY, ReadableType.QUIZ.value)
                    }
                }, R.id.fragment_container_quiz)
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED, sticky = true)
    open fun onStuffReadEvent(event: StuffReadEvent) {
        val stickyEvent = EventBus.getDefault().removeStickyEvent(StuffReadEvent::class.java)
        if (stickyEvent != null) {
            quizViewModel.findStuffById(event.stuffId)
            quizViewModel.readStuff.observe(viewLifecycleOwner, Observer { stuff ->
                stuff.task?.let {
                    EventBus.getDefault().postSticky(
                        TaskRequestedEvent(
                            stuff.stuff.stuffId,
                            it.taskType
                        )
                    )
                }
            })
        }
    }
}
