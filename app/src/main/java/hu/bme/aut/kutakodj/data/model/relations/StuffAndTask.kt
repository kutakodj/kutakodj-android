package hu.bme.aut.kutakodj.data.model.relations

import androidx.room.Embedded
import androidx.room.Relation
import hu.bme.aut.kutakodj.data.model.Stuff
import hu.bme.aut.kutakodj.data.model.Task

data class StuffAndTask(
    @Embedded val stuff: Stuff,
    @Relation(
        parentColumn = "stuffId",
        entityColumn = "stuffOwnerId"
    )
    val task: Task?
)
