package hu.bme.aut.kutakodj.view.adapter.itemdecoration

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration

class GridSpacingItemDecoration(
    private val spanCount: Int,
    private val spacing: Int,
    private val includeEdge: Boolean,
    private val headerNumber: Int
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val itemPosition = parent.getChildAdapterPosition(view) - headerNumber

        if (itemPosition >= 0) {
            val itemColumn = itemPosition % spanCount
            if (includeEdge) {
                outRect.left = spacing - itemColumn * spacing / spanCount
                outRect.right = (itemColumn + 1) * spacing / spanCount

                if (itemPosition < spanCount) {
                    outRect.top = spacing
                }
                outRect.bottom = spacing
            } else {
                outRect.left = itemColumn * spacing / spanCount
                outRect.right = spacing - (itemColumn + 1) * spacing / spanCount

                if (itemPosition >= spanCount) {
                    outRect.top = spacing
                }
            }
        } else {
            outRect.left = 0
            outRect.right = 0
            outRect.top = 0
            outRect.bottom = 0
        }
    }
}
