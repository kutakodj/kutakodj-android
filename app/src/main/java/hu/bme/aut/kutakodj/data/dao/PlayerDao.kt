package hu.bme.aut.kutakodj.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import hu.bme.aut.kutakodj.data.model.Player
import hu.bme.aut.kutakodj.data.model.relations.PlayerWithRooms

@Dao
interface PlayerDao : BaseDao<Player> {
    @Transaction
    @Query("Select * from player_table LIMIT 1")
    fun getAll(): LiveData<PlayerWithRooms>

    @Transaction
    @Query("Select * from player_table where playerId = (:playerId)")
    suspend fun getById(playerId: Long): PlayerWithRooms
}
