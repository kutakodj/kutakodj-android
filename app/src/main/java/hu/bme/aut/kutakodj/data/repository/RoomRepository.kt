package hu.bme.aut.kutakodj.data.repository

import hu.bme.aut.kutakodj.data.dao.RoomDao
import hu.bme.aut.kutakodj.data.model.Room
import hu.bme.aut.kutakodj.data.model.relations.RoomWithRewardsAndStuffs

class RoomRepository(private val dao: RoomDao) {
    val allRooms = dao.getAllLiveData()

    suspend fun getAll(): List<RoomWithRewardsAndStuffs> = dao.getAll()

    suspend fun getById(id: Long): RoomWithRewardsAndStuffs = dao.getById(id)

    suspend fun getByName(roomName: String): RoomWithRewardsAndStuffs = dao.getByName(roomName)

    suspend fun insertAll(vararg obj: Room) = dao.insertAll(*obj)

    suspend fun insert(obj: Room) = dao.insert(obj)

    suspend fun update(obj: Room) = dao.update(obj)

    suspend fun delete(obj: Room) = dao.delete(obj)
}
