package hu.bme.aut.kutakodj.view.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.view.adapter.CollectionPagerAdapter
import kotlinx.android.synthetic.main.fragment_main.*

/*
Fragment used instead of CollectionActivity
This holds the main game fragments
 */
class MainFragment : Fragment(R.layout.fragment_main) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vp2_collection.adapter = CollectionPagerAdapter(this)
        vp2_collection.setCurrentItem(1, false)
        /*
         Disable saving state to solve crash
         Source: https://stackoverflow.com/a/59804052
         */
        vp2_collection.isSaveEnabled = false
    }
}