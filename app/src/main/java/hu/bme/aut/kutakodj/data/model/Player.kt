package hu.bme.aut.kutakodj.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "player_table")
data class Player(
    @PrimaryKey(autoGenerate = true) val playerId: Long,
    val name: String,
    var points: Long,
    var code: String,
    var puzzleOK: Boolean
)
