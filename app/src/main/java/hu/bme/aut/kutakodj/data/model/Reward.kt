package hu.bme.aut.kutakodj.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "reward_table")
data class Reward(
    @PrimaryKey(autoGenerate = true) val rewardId: Long,
    val roomOwnerId: Long,
    val name: String
)
