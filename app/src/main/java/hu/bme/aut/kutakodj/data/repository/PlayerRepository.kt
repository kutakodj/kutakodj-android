package hu.bme.aut.kutakodj.data.repository

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import hu.bme.aut.kutakodj.data.dao.PlayerDao
import hu.bme.aut.kutakodj.data.model.Player
import hu.bme.aut.kutakodj.data.model.relations.PlayerWithRooms


class PlayerRepository(private val dao: PlayerDao, private val preferences: SharedPreferences) {
    val allItems: LiveData<PlayerWithRooms> = dao.getAll()

    companion object {
        private const val KEY_APP_STARTED = "appStarted"
    }

    var appStarted: Boolean
        get() = preferences.getBoolean(KEY_APP_STARTED, false)
        set(value) = preferences.edit().putBoolean(KEY_APP_STARTED, value).apply()

    suspend fun getById(id: Long): PlayerWithRooms = dao.getById(id)

    suspend fun insertAll(vararg obj: Player) = dao.insertAll(*obj)

    suspend fun insert(obj: Player) = dao.insert(obj)

    suspend fun update(obj: Player) = dao.update(obj)

    suspend fun delete(obj: Player) = dao.delete(obj)
}
