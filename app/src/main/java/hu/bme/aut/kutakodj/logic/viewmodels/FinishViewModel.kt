package hu.bme.aut.kutakodj.logic.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import hu.bme.aut.kutakodj.data.model.relations.PlayerWithRooms
import hu.bme.aut.kutakodj.logic.interactors.PlayerInteractor
import kotlinx.coroutines.launch

class FinishViewModel(
    application: Application,
    private val playerInteractor: PlayerInteractor
) : AndroidViewModel(application) {
    private val _player = MutableLiveData<PlayerWithRooms>()
    val player: MediatorLiveData<String> = MediatorLiveData()

    init {
        player.addSource(_player) {
            player.value = it.player.code
        }

        loadPlayer()
    }

    private fun loadPlayer() = viewModelScope.launch {
        _player.postValue(playerInteractor.getActivePlayer())
    }

    fun assignCodeToPlayer() = viewModelScope.launch {
        playerInteractor.assignCodeToPlayer()
    }

    fun checkPlayerCode(code: String) = viewModelScope.launch {
        playerInteractor.checkPlayerCode(code)
    }
}
