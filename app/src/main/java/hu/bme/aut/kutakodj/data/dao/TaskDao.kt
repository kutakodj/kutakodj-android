package hu.bme.aut.kutakodj.data.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import hu.bme.aut.kutakodj.data.model.Task
import hu.bme.aut.kutakodj.data.model.relations.TaskWithQuestions

@Dao
interface TaskDao : BaseDao<Task> {
    @Transaction
    @Query("Select * from task_table")
    suspend fun getAll(): List<TaskWithQuestions>

    @Transaction
    @Query("Select * from task_table where taskId = (:taskId)")
    suspend fun getById(taskId: Long): TaskWithQuestions

    @Transaction
    @Query("Delete from task_table")
    suspend fun deleteAll()
}
