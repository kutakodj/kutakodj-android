package hu.bme.aut.kutakodj.data

import android.content.Context
import android.content.SharedPreferences
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.data.dao.*
import hu.bme.aut.kutakodj.data.database.MuseumDatabase
import hu.bme.aut.kutakodj.data.repository.*
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val dataModule = module {
    single<MuseumDatabase> { MuseumDatabase.getDatabase(androidContext()) }

    single<AchievementDao> { get<MuseumDatabase>().AchievementDao() }
    single<AnswerDao> { get<MuseumDatabase>().AnswerDao() }
    single<PlayerDao> { get<MuseumDatabase>().PlayerDao() }
    single<QuestionDao> { get<MuseumDatabase>().QuestionDao() }
    single<RewardDao> { get<MuseumDatabase>().RewardDao() }
    single<RoomDao> { get<MuseumDatabase>().RoomDao() }
    single<RuleDao> { get<MuseumDatabase>().RuleDao() }
    single<StuffDao> { get<MuseumDatabase>().StuffDao() }
    single<TaskDao> { get<MuseumDatabase>().TaskDao() }

    single { AchievementRepository(get()) }
    single { AnswerRepository(get()) }
    single { PlayerRepository(get(), get()) }
    single { QuestionRepository(get()) }
    single { RewardRepository(get()) }
    single { RoomRepository(get()) }
    single { RuleRepository(get()) }
    single { StuffRepository(get()) }
    single { TaskRepository(get()) }

    single<SharedPreferences> {
        androidApplication().getSharedPreferences(androidContext().getString(R.string.app_name), Context.MODE_PRIVATE)
    }
}