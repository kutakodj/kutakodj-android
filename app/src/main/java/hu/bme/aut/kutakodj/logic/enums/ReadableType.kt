package hu.bme.aut.kutakodj.logic.enums

enum class ReadableType(val value: Int) {
    WELL(0), QUIZ(1), PUZZLE(2);
}

fun Int.toReadableType(): ReadableType? {
    return ReadableType.values().find { it.value == this }
}