package hu.bme.aut.kutakodj.view.fragment

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.logic.viewmodels.CollectionViewModel
import hu.bme.aut.kutakodj.navigation.getNavOwner
import hu.bme.aut.kutakodj.service.AchievementService
import hu.bme.aut.kutakodj.util.switchStatusBarColor
import hu.bme.aut.kutakodj.view.adapter.CollectionGridAdapter
import hu.bme.aut.kutakodj.view.adapter.itemdecoration.GridSpacingItemDecoration
import hu.bme.aut.kutakodj.view.fragment.FinishFragment.Companion.STATE_KEY
import kotlinx.android.synthetic.main.fragment_grid.*
import org.koin.androidx.viewmodel.ext.android.getViewModel

class CollectionFragment : Fragment() {

    private lateinit var collectionViewModel: CollectionViewModel
    private lateinit var achievementService: AchievementService
    private var serviceIsBound: Boolean = false

    private object Animator {
        const val NOT_STARTED = 0
        const val STARTED = 1
    }

    private val connection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            val binder = service as AchievementService.LocalBinder
            achievementService = binder.getService()
            serviceIsBound = true
            achievementService.achievements.observe(viewLifecycleOwner, Observer { achi ->
                if (achi.isNotEmpty()) {
                    if (achi.all { it.achievement.acquired }) {
                        btn_points.text = "OK"
                        btn_points.setOnClickListener {
                            showFinishDialog(FinishFragment.Companion.FinishState.FINISH.value)
                        }
                    } else if (achi[0].achievement.acquired) {
                        btn_points.text = "OK"
                        btn_points.setOnClickListener {
                            showFinishDialog(FinishFragment.Companion.FinishState.PUZZLE.value)
                        }
                    }
                }
            })
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            serviceIsBound = false
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_grid, container, false)

        activity?.switchStatusBarColor(R.color.bgColorPrimaryDark)

        collectionViewModel = getViewModel()

        collectionViewModel.visibleRewards.observe(viewLifecycleOwner, Observer { visibilityList ->
            if (visibilityList.all { !it }) {
                view_animator_collection.displayedChild = Animator.NOT_STARTED
            } else {
                view_animator_collection.displayedChild = Animator.STARTED

                val adapter = activity?.let {
                    CollectionGridAdapter(it)
                }

                adapter?.setVisibilities(visibilityList)

                initRecyclerView(view, adapter)

                collectionViewModel.rewards.observe(viewLifecycleOwner, Observer { rewards ->
                    rewards?.let { list ->
                        adapter?.let {
                            adapter.setItems(list)
                        }
                    }
                })
            }
        })

        initButtons(view)
        return view
    }

    private fun initRecyclerView(view: View, adapter: CollectionGridAdapter?) {
        val recyclerView = view.findViewById<RecyclerView>(R.id.reward_recyclerview)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = activity?.let {
            GridLayoutManager(it, 3)
        }
        val spacing = resources.getDimensionPixelSize(R.dimen.grid_reward_margin)
        if (recyclerView.itemDecorationCount == 0) recyclerView.addItemDecoration(GridSpacingItemDecoration(3, spacing, true, 0))
    }

    private fun initButtons(view: View) {
        val btn_info: MaterialButton = view.findViewById(R.id.btn_info)
        val btn_points: MaterialButton = view.findViewById(R.id.btn_points)
        btn_info.setBackgroundResource(R.drawable.shape_round_right)
        btn_points.setBackgroundResource(R.drawable.shape_round_left)

        btn_info.setOnClickListener {
            requireActivity()
                .getNavOwner()
                ?.addFragment(InfoChoiceFragment(), R.id.main_content)
        }

        collectionViewModel.points.observe(viewLifecycleOwner, Observer {
            btn_points.text = it
        })
    }

    private fun showFinishDialog(state: String) {
        val dialogFragment = FinishFragment().apply {
            arguments = Bundle().apply {
                this.putString(STATE_KEY, state)
            }
        }
        requireActivity()
            .getNavOwner()
            ?.addFragment(dialogFragment, R.id.main_content)
        //dialogFragment.show(childFragmentManager, "finish")
    }

    override fun onStart() {
        super.onStart()
        Intent(requireContext(), AchievementService::class.java).also { intent ->
            requireContext().bindService(intent, connection, Context.BIND_AUTO_CREATE)
        }
    }

    override fun onResume() {
        activity?.switchStatusBarColor(R.color.bgColorPrimaryDark)
        super.onResume()
    }

    override fun onStop() {
        super.onStop()
        if (serviceIsBound) {
            requireContext().unbindService(connection)
            serviceIsBound = false
        }
    }
}
