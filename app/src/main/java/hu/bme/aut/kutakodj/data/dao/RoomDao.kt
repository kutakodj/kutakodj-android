package hu.bme.aut.kutakodj.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import hu.bme.aut.kutakodj.data.model.Room
import hu.bme.aut.kutakodj.data.model.relations.RoomWithRewardsAndStuffs

@Dao
interface RoomDao : BaseDao<Room> {

    @Transaction
    @Query("Select * from room_table")
    fun getAllLiveData(): LiveData<List<RoomWithRewardsAndStuffs>>

    @Transaction
    @Query("Select * from room_table")
    suspend fun getAll(): List<RoomWithRewardsAndStuffs>

    @Transaction
    @Query("Select * from room_table where roomId = (:roomId)")
    suspend fun getById(roomId: Long): RoomWithRewardsAndStuffs

    @Transaction
    @Query("Select * from room_table where name = (:roomName)")
    suspend fun getByName(roomName: String): RoomWithRewardsAndStuffs
}
