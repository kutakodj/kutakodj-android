package hu.bme.aut.kutakodj.view.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.events.navigation.StuffReadEvent
import hu.bme.aut.kutakodj.events.navigation.TaskRequestedEvent
import hu.bme.aut.kutakodj.logic.enums.ReadableType
import hu.bme.aut.kutakodj.logic.viewmodels.WellViewModel
import hu.bme.aut.kutakodj.navigation.getNavOwner
import hu.bme.aut.kutakodj.view.adapter.WellListAdapter
import kotlinx.android.synthetic.main.fragment_well.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.koin.androidx.viewmodel.ext.android.getViewModel

class WellFragment : Fragment(R.layout.fragment_well) {

    private lateinit var wellViewModel: WellViewModel
    private lateinit var adapter: WellListAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()

        wellViewModel = getViewModel()
        wellViewModel.loadRoomByName("Well")
        wellViewModel.wellStuffs.observe(viewLifecycleOwner, Observer { stuffs ->
            stuffs?.let { list ->
                adapter.setItems(list)
            }
        })
    }

    private fun initRecyclerView() {
        adapter = WellListAdapter(requireActivity())
        adapter.navigationOwner = requireActivity().getNavOwner()
        recyclerview_well.adapter = adapter
        recyclerview_well.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
    }

    override fun onResume() {
        super.onResume()
        civ_action_well.setOnClickListener {
            requireActivity()
                .getNavOwner()
                ?.replaceFragment(ReadFragment().apply {
                    arguments = Bundle().apply {
                        this.putInt(ReadFragment.READABLE_TYPE_KEY, ReadableType.WELL.value)
                    }
                }, R.id.fragment_container_well)
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED, sticky = true)
    open fun onStuffReadEvent(event: StuffReadEvent) {
        val stickyEvent = EventBus.getDefault().removeStickyEvent(StuffReadEvent::class.java)
        if (stickyEvent != null) {
            wellViewModel.findStuffById(event.stuffId)
            if (!wellViewModel.currentStuff.hasActiveObservers()) {
                wellViewModel.currentStuff.observe(viewLifecycleOwner, Observer { stuff ->
                    stuff.task?.let {
                        EventBus.getDefault().postSticky(
                            TaskRequestedEvent(
                                stuff.stuff.stuffId,
                                it.taskType
                            )
                        )
                    }
                })
            }
        }
    }
}
