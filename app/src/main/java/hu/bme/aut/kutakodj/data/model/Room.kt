package hu.bme.aut.kutakodj.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "room_table")
data class Room(
    @PrimaryKey(autoGenerate = true) val roomId: Long,
    val playerOwnerId: Long,
    val name: String,
    var visited: Boolean
)
