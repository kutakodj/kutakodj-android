package hu.bme.aut.kutakodj.navigation

import androidx.fragment.app.Fragment

interface NavigationOwner {
    val backstacks: Stack<Pair<Int, Stack<NavBundle>>>
    var currentFragment: Pair<Int, NavBundle>?

    fun addFragment(
        fragment: Fragment,
        containerId: Int,
        backStackMoveType: BackStackMoveType = BackStackMoveType.NONE
    )

    fun replaceFragment(fragment: Fragment, containerId: Int)
    fun backPress()
}