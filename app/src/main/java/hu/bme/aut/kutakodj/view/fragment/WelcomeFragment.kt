package hu.bme.aut.kutakodj.view.fragment

import androidx.fragment.app.Fragment
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.data.repository.PlayerRepository
import hu.bme.aut.kutakodj.navigation.getNavOwner
import kotlinx.android.synthetic.main.fragment_welcome.*
import org.koin.android.ext.android.inject

class WelcomeFragment : Fragment(R.layout.fragment_welcome) {

    private val prevStarted = "prevStarted"
    private val playerRepository: PlayerRepository by inject()

    override fun onResume() {
        super.onResume()
        if (!playerRepository.appStarted) {
            playerRepository.appStarted = true
            civ_mainStartLogo.setOnClickListener {
                requireActivity()
                    .getNavOwner()
                    ?.replaceFragment(InfoPanelHolderFragment(), R.id.main_content)
            }
        } else {
            civ_mainStartLogo.setOnClickListener {
                requireActivity()
                    .getNavOwner()
                    ?.addFragment(MainFragment(), R.id.main_content)
            }
        }
    }
}