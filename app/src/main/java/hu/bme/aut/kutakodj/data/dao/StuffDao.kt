package hu.bme.aut.kutakodj.data.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import hu.bme.aut.kutakodj.data.model.Stuff
import hu.bme.aut.kutakodj.data.model.relations.StuffAndTask

@Dao
interface StuffDao : BaseDao<Stuff> {
    @Transaction
    @Query("Select * from stuff_table")
    suspend fun getAll(): List<StuffAndTask>

    @Transaction
    @Query("Select * from stuff_table where stuffId = (:stuffId)")
    suspend fun getById(stuffId: Long): StuffAndTask

    @Transaction
    @Query("Select * from stuff_table where containingRoomId = (:id)")
    suspend fun getByRoomId(id: Long): List<StuffAndTask>

    @Transaction
    @Query("Delete from stuff_table")
    suspend fun deleteAll()
}
