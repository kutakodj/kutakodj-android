package hu.bme.aut.kutakodj.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "question_table")
data class Question(
    @PrimaryKey(autoGenerate = true) val questionId: Long,
    val questionText: String,
    val taskOwnerId: Long,
    val correctAnswerId: Long,
    val type: Long
)
