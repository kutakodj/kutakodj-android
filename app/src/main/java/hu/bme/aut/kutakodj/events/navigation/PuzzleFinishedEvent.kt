package hu.bme.aut.kutakodj.events.navigation

data class PuzzleFinishedEvent(val playerCode: String)