package hu.bme.aut.kutakodj.view.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import hu.bme.aut.kutakodj.view.fragment.CollectionFragment
import hu.bme.aut.kutakodj.view.fragment.QuizBaseFragment
import hu.bme.aut.kutakodj.view.fragment.WellBaseFragment

class CollectionPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    private val sites: MutableList<Fragment> = mutableListOf()

    init {
        sites.add(QuizBaseFragment())
        sites.add(CollectionFragment())
        sites.add(WellBaseFragment())
    }

    override fun getItemCount(): Int = sites.size

    override fun createFragment(position: Int): Fragment {
        return if (position < sites.size && position >= 0) sites[position]
        else throw IllegalArgumentException("No such page!")
    }

    override fun getItemId(position: Int): Long {
        return sites[position].hashCode().toLong()
    }
}
