package hu.bme.aut.kutakodj.logic.enums

enum class ValidatorType {
    MOBILE, PUZZLE
}