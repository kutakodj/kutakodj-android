package hu.bme.aut.kutakodj.logic.helpers

import android.content.Intent
import android.nfc.NdefMessage
import android.nfc.NfcAdapter

object NFCHelper {

    fun processNFCData(inputIntent: Intent): String {
        val rawMessages =
            inputIntent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)
        var data = ""
        if (rawMessages != null && rawMessages.isNotEmpty()) {
            val messages = arrayOfNulls<NdefMessage>(rawMessages.size)
            for (i in rawMessages.indices) {
                messages[i] = rawMessages[i] as NdefMessage
            }
            // Log.i("MESSAGE", "message size = " + messages.size)

            // only one message sent during the Android beam
            // so you can just grab the first record.
            val msg = rawMessages[0] as NdefMessage

            // record 0 contains the MIME type, record 1 is the AAR, if present
            val payloadStringData = String(msg.records[0].payload)

            // now do something with your payload payloadStringData
            data = payloadStringData
        }
        return data
    }
}
