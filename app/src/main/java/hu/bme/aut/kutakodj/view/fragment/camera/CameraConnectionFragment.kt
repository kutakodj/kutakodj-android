package hu.bme.aut.kutakodj.view.fragment.camera

import android.content.Context
import android.content.res.Configuration
import android.graphics.ImageFormat
import android.graphics.Matrix
import android.graphics.RectF
import android.graphics.SurfaceTexture
import android.hardware.camera2.*
import android.media.ImageReader
import android.os.Handler
import android.os.HandlerThread
import android.util.Log
import android.util.Size
import android.view.Surface
import android.view.TextureView
import androidx.fragment.app.Fragment
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.logic.helpers.CameraHelper
import hu.bme.aut.kutakodj.util.log
import hu.bme.aut.kutakodj.util.showToast
import kotlinx.android.synthetic.main.fragment_camera.*
import java.util.concurrent.Semaphore
import java.util.concurrent.TimeUnit

open class CameraConnectionFragment private constructor(
    private val cameraConnectionCallback: ConnectionCallback,
    private val imageListener: ImageReader.OnImageAvailableListener,
    private val inputSize: Size
) : Fragment(R.layout.fragment_camera) {

    companion object {
        fun newInstance(
            cameraConnectionCallback: ConnectionCallback,
            imageListener: ImageReader.OnImageAvailableListener,
            inputSize: Size
        ): CameraConnectionFragment {
            return CameraConnectionFragment(cameraConnectionCallback, imageListener, inputSize)
        }
    }

    private val cameraOpenCloseLock = Semaphore(1)

    private var cameraId: String? = null
    private var captureSession: CameraCaptureSession? = null
    private var cameraDevice: CameraDevice? = null
    private var sensorOrientation: Int = 0
    private var previewSize: Size? = null

    private var backgroundThread: HandlerThread? = null
    private var backgroundHandler: Handler? = null

    private var previewReader: ImageReader? = null
    private var previewRequestBuilder: CaptureRequest.Builder? = null
    private var previewRequest: CaptureRequest? = null
    private val stateCallback = object : CameraDevice.StateCallback() {
        override fun onOpened(cameraDev: CameraDevice) {
            // This method is called when the camera is opened.  We start camera preview here.
            cameraOpenCloseLock.release()
            cameraDevice = cameraDev
            createCameraPreviewSession()
        }

        override fun onDisconnected(cameraDev: CameraDevice) {
            cameraOpenCloseLock.release()
            cameraDev.close()
            cameraDevice = null
        }

        override fun onError(cameraDev: CameraDevice, error: Int) {
            cameraOpenCloseLock.release()
            cameraDev.close()
            cameraDevice = null
            activity?.finish()
        }

    }

    private val captureCallback = object : CameraCaptureSession.CaptureCallback() {}
    private val captureSessionStateCallback = object : CameraCaptureSession.StateCallback() {
        override fun onConfigureFailed(cameraCaptureSession: CameraCaptureSession) {
            activity.showToast("Camera configure FAILED!")
        }

        override fun onConfigured(cameraCaptureSession: CameraCaptureSession) {
            // The camera is already closed
            if (cameraDevice == null) return

            // When the session is ready, we start displaying the preview.
            captureSession = cameraCaptureSession
            try {
                // Auto focus should be continuous for camera preview.
                previewRequestBuilder?.set(
                    CaptureRequest.CONTROL_AF_MODE,
                    CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE)

                // Flash is automatically enabled when necessary.
                previewRequestBuilder?.set(
                    CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH)

                // Finally, we start displaying the camera preview.
                previewRequest = previewRequestBuilder?.build()
                previewRequest?.let {
                    captureSession?.setRepeatingRequest(
                        it, captureCallback, backgroundHandler)
                }
            } catch (e: CameraAccessException) {
                e.log("CAMERA PREVIEW")
            }
        }

    }

    private val surfaceTextureListener = object : TextureView.SurfaceTextureListener {
        override fun onSurfaceTextureSizeChanged(texture: SurfaceTexture, width: Int, height: Int) {
            configureTransform(width, height)
        }

        override fun onSurfaceTextureUpdated(p0: SurfaceTexture) {}

        override fun onSurfaceTextureDestroyed(p0: SurfaceTexture): Boolean = true

        override fun onSurfaceTextureAvailable(surface: SurfaceTexture, width: Int, height: Int) {
            openCamera(width, height)
        }
    }

    override fun onResume() {
        super.onResume()

        startBackgroundThread()
        // When the screen is turned off and turned back on, the SurfaceTexture is already
        // available, and "onSurfaceTextureAvailable" will not be called. In that case, we can open
        // a camera and start preview from here (otherwise, we wait until the surface is ready in
        // the SurfaceTextureListener).
        if (texture.isAvailable) {
            openCamera(texture.width, texture.height)
        } else {
            texture.surfaceTextureListener = surfaceTextureListener
        }
    }

    override fun onPause() {
        closeCamera()
        stopBackgroundThread()
        super.onPause()
    }

    fun setCamera(paramCameraId: String) {
        cameraId = paramCameraId
    }

    /** Sets up member variables related to camera. */
    private fun setUpCameraOutputs() {
        val cameraManager =
            requireActivity().getSystemService(Context.CAMERA_SERVICE) as CameraManager
        val immutableCameraId = cameraId ?: return
        try {
            val characteristics = cameraManager.getCameraCharacteristics(immutableCameraId)
            val map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)
            sensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION) ?: 0

            // Danger, W.R.! Attempting to use too large a preview size could  exceed the camera
            // bus' bandwidth limitation, resulting in gorgeous previews but the storage of
            // garbage capture data.
            previewSize = CameraHelper.chooseOptimalSize(
                map?.getOutputSizes(SurfaceTexture::class.java)?.toList() ?: emptyList(),
                inputSize.width,
                inputSize.height
            )

            // We fit the aspect ratio of TextureView to the size of preview we picked.
            val orientation = resources.configuration.orientation
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                texture.setAspectRatio(previewSize?.width ?: 0, previewSize?.height ?: 0)
            } else {
                texture.setAspectRatio(previewSize?.height ?: 0, previewSize?.width ?: 0)
            }
        } catch (e: CameraAccessException) {
            e.log("CAMERA_ACCESS")
        } catch (e: NullPointerException) {
            // Currently an NPE is thrown when the Camera2API is used but not supported on the
            // device this code runs.
            e.log("CAMERA_NULLPTR")
            activity.showToast(getString(R.string.camera_error))
            throw IllegalStateException(getString(R.string.camera_error))
        }
        previewSize?.let { cameraConnectionCallback.onPreviewSizeChosen(it, sensorOrientation) }
    }

    /** Opens the camera specified by {@link CameraConnectionFragment#cameraId}. */
    private fun openCamera(width: Int, height: Int) = runWithPermissions(android.Manifest.permission.CAMERA) {
        setUpCameraOutputs()
        configureTransform(width, height)
        val cameraManager =
            requireActivity().getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            if (!cameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                throw RuntimeException("Time out waiting to lock camera opening.")
            }

            cameraId?.let {
                cameraManager.openCamera(it, stateCallback, backgroundHandler)
            }
        } catch (e: SecurityException) {
            e.log("OPEN_CAMERA")
        } catch (e: InterruptedException) {
            throw RuntimeException("Interrupted while trying to lock camera opening.", e)
        }
    }

    /** Closes the current {@link CameraDevice}. */
    private fun closeCamera() {
        try {
            cameraOpenCloseLock.acquire()
            captureSession?.close()
            captureSession = null
            cameraDevice?.close()
            cameraDevice = null
            previewReader?.close()
            previewReader = null
        } catch (e: InterruptedException) {
            throw RuntimeException("Interrupted while trying to lock camera closing.", e)
        } finally {
            cameraOpenCloseLock.release()
        }
    }

    /** Starts a background thread and its {@link Handler}. */
    private fun startBackgroundThread() {
        backgroundThread = HandlerThread("ImageListener")
        backgroundThread?.start()
        backgroundHandler = backgroundThread?.looper?.let { Handler(it) }
    }

    /** Stops the background thread and its {@link Handler}. */
    private fun stopBackgroundThread() {
        backgroundThread?.quitSafely()
        try {
            backgroundThread?.join()
            backgroundThread = null
            backgroundHandler = null
        } catch (e: InterruptedException) {
            e.log("BGTHREAD")
        }
    }

    /** Creates a new {@link CameraCaptureSession} for camera preview. */
    private fun createCameraPreviewSession() {
        try {
            texture.surfaceTexture?.let { surfaceText ->
                // We configure the size of default buffer to be the size of camera preview we want.
                previewSize?.let {
                    surfaceText.setDefaultBufferSize(it.width, it.height)
                }

                // This is the output Surface we need to start preview.
                val surface = Surface(surfaceText)

                // We set up a CaptureRequest.Builder with the output Surface.
                cameraDevice?.let {
                    previewRequestBuilder = it.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)
                    previewRequestBuilder?.addTarget(surface)
                }

                Log.d("CAMERA PREVIEW",
                    "Opening camera preview: ${previewSize?.width} x ${previewSize?.height}")

                // Create the reader for the preview frames.

                previewSize?.let {
                    previewReader = ImageReader.newInstance(
                        it.width, it.height, ImageFormat.YUV_420_888, 2
                    )

                }
                previewReader?.let {
                    it.setOnImageAvailableListener(imageListener, backgroundHandler)
                    previewRequestBuilder?.addTarget(it.surface)
                }

                // Here, we create a CameraCaptureSession for camera preview.
                cameraDevice?.let {
                    it.createCaptureSession(listOf(surface, previewReader?.surface), captureSessionStateCallback, null)
                }
            }
        } catch (e: CameraAccessException) {
            e.log("CAMERA PREVIEW SESSION")
        }
    }

    /**
     * Configures the necessary Matrix transformation to TextureView. This method should be
     * called after the camera preview size is determined in setUpCameraOutputs and also the size of
     * TextureView is fixed.
     *
     * @param viewWidth The width of TextureView
     * @param viewHeight The height of TextureView
     */
    private fun configureTransform(viewWidth: Int, viewHeight: Int) {
        val innerTexture = texture
        val innerPreviewSize = previewSize
        if (innerTexture == null || innerPreviewSize == null) return
        else {
            val rotation = requireActivity().windowManager.defaultDisplay.rotation
            val matrix = Matrix()
            val viewRect = RectF(0F, 0F, viewWidth.toFloat(), viewHeight.toFloat())
            val bufferRect = RectF(0F, 0F, innerPreviewSize.height.toFloat(), innerPreviewSize.width.toFloat())

            when (rotation) {
                Surface.ROTATION_90, Surface.ROTATION_270 -> {
                    bufferRect.offset(
                        viewRect.centerX() - bufferRect.centerX(),
                        viewRect.centerY() - bufferRect.centerY())
                    matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL)
                    val scale = maxOf(
                        viewHeight.toFloat() / innerPreviewSize.height.toFloat(),
                        viewWidth.toFloat() / innerPreviewSize.width.toFloat())
                    matrix.postScale(scale, scale, viewRect.centerX(), viewRect.centerY())
                    matrix.postRotate(90 * (rotation - 2).toFloat(), viewRect.centerX(), viewRect.centerY())
                }
                Surface.ROTATION_180 -> {
                    matrix.postRotate(180F, viewRect.centerX(), viewRect.centerY())
                }
            }
            innerTexture.setTransform(matrix)
        }
    }

    interface ConnectionCallback {
        fun onPreviewSizeChosen(size: Size, cameraRotation: Int)
    }
}