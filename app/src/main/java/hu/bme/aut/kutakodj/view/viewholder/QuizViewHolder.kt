package hu.bme.aut.kutakodj.view.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textview.MaterialTextView
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.data.model.relations.StuffAndTask

class QuizViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val tv_quizCount: MaterialTextView = itemView.findViewById(R.id.tv_quizCount)

    var item: StuffAndTask? = null
}
