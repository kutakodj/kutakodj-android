package hu.bme.aut.kutakodj.logic.tflite

import android.graphics.RectF

data class Recognition(
    val id: String,
    val title: String,
    val confidence: Float,
    val location: RectF?
)