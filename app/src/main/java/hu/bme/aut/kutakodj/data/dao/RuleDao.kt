package hu.bme.aut.kutakodj.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import hu.bme.aut.kutakodj.data.model.Rule

@Dao
interface RuleDao : BaseDao<Rule> {
    @Transaction
    @Query("Select * from rule_table")
    fun getAll(): LiveData<List<Rule>>

    @Transaction
    @Query("Select * from rule_table")
    suspend fun getAllSimple(): List<Rule>

    @Transaction
    @Query("Select * from rule_table where ruleId = (:ruleId)")
    fun getById(ruleId: Long): LiveData<Rule>
}
