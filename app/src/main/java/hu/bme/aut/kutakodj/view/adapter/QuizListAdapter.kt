package hu.bme.aut.kutakodj.view.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.data.model.relations.StuffAndTask
import hu.bme.aut.kutakodj.view.viewholder.QuizViewHolder

class QuizListAdapter internal constructor(private val context: Context) : RecyclerView.Adapter<QuizViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var stuffs = emptyList<StuffAndTask>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuizViewHolder {
        val itemView = inflater.inflate(R.layout.listitem_quiz, parent, false)
        return QuizViewHolder(itemView)
    }

    override fun getItemCount(): Int = stuffs.size

    override fun onBindViewHolder(holder: QuizViewHolder, position: Int) {
        val item = stuffs[position]
        Log.i("QUIZ", "$item")
        holder.tv_quizCount.text = context.resources.getString(R.string.quiz_position_text, item.stuff.stuffId)
        holder.item = item
    }

    fun setItems(items: List<StuffAndTask>) {
        stuffs = items
        notifyDataSetChanged()
    }
}
