package hu.bme.aut.kutakodj.logic.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import hu.bme.aut.kutakodj.data.model.relations.StuffAndTask
import hu.bme.aut.kutakodj.logic.interactors.RoomInteractor
import kotlinx.coroutines.launch

class QuizViewModel(
    application: Application,
    private val roomInteractor: RoomInteractor
    ) : AndroidViewModel(application) {

    private var _quizStuffs =  MutableLiveData<List<StuffAndTask>>()
    val quizStuffs: LiveData<List<StuffAndTask>> = _quizStuffs

    private var _readStuff = MutableLiveData<StuffAndTask>()
    val readStuff: LiveData<StuffAndTask> = _readStuff

    fun loadRoomByName(roomName: String) = viewModelScope.launch {
        _quizStuffs.postValue(roomInteractor.getAllItemsOfRoom(roomName))
    }

    fun findStuffById(stuffId: Long) = viewModelScope.launch {
        _readStuff.postValue(roomInteractor.getStuffById(stuffId))
    }
}
