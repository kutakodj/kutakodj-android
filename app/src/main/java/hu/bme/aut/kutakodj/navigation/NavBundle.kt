package hu.bme.aut.kutakodj.navigation

import androidx.fragment.app.Fragment

data class NavBundle(
    val fragment: Fragment,
    var putOnBackStack: Boolean
)