package hu.bme.aut.kutakodj.view.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.navigation.BackStackMoveType
import hu.bme.aut.kutakodj.navigation.getNavOwner
import hu.bme.aut.kutakodj.view.adapter.InfoPagerAdapter
import kotlinx.android.synthetic.main.fragment_info_panel_holder.*

class InfoPanelHolderFragment : Fragment(R.layout.fragment_info_panel_holder) {

    private var selectedIndex: Int = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = InfoPagerAdapter(this)
        vp2_info.adapter = adapter
        vp2_info.isSaveEnabled = false
        dots_indicator.setViewPager2(vp2_info)

        vp2_info.registerOnPageChangeCallback(object : OnPageChangeCallback() {

            override fun onPageSelected(position: Int) {
                selectedIndex = position
                super.onPageSelected(position)
            }

            override fun onPageScrollStateChanged(state: Int) {
                if (state == ViewPager2.SCROLL_STATE_DRAGGING) {
                    if (selectedIndex == adapter.itemCount - 1) {
                        launchCollectionActivity()
                    }
                }
            }
        })
    }

    private fun launchCollectionActivity() {
        requireActivity()
            .getNavOwner()
            ?.addFragment(MainFragment(), R.id.main_content, BackStackMoveType.TOP)
    }
}
