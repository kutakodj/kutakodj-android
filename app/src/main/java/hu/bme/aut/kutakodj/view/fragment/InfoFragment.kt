package hu.bme.aut.kutakodj.view.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.DrawableImageViewTarget
import hu.bme.aut.kutakodj.R
import jp.wasabeef.glide.transformations.CropCircleWithBorderTransformation
import kotlinx.android.synthetic.main.fragment_info.*

class InfoFragment : Fragment(R.layout.fragment_info) {

    companion object {
        const val KEY_DESCRIPTION = "description"
        const val KEY_GIF = "gif"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        tv_infoDescription.text = arguments?.getString(KEY_DESCRIPTION)
        val target = DrawableImageViewTarget(civ_infoAnim)
        Glide
            .with(this)
            .load(arguments?.getInt(KEY_GIF))
            .apply(RequestOptions
                .bitmapTransform(CropCircleWithBorderTransformation(10, resources.getColor(R.color.borderColorImageLight))))
            .into(target)
        super.onViewCreated(view, savedInstanceState)
    }
}
