package hu.bme.aut.kutakodj.data.repository

import androidx.lifecycle.LiveData
import hu.bme.aut.kutakodj.data.dao.RuleDao
import hu.bme.aut.kutakodj.data.model.Rule

class RuleRepository(private val dao: RuleDao) {
    val allItems: LiveData<List<Rule>> = dao.getAll()

    fun getById(id: Long): LiveData<Rule> = dao.getById(id)

    suspend fun getAll(): List<Rule> = dao.getAllSimple()

    suspend fun insertAll(vararg obj: Rule) = dao.insertAll(*obj)

    suspend fun insert(obj: Rule) = dao.insert(obj)

    suspend fun update(obj: Rule) = dao.update(obj)

    suspend fun delete(obj: Rule) = dao.delete(obj)
}
