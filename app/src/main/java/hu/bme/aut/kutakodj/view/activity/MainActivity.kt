package hu.bme.aut.kutakodj.view.activity

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import androidx.fragment.app.Fragment
import hu.bme.aut.kutakodj.Constants.isQR
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.events.navigation.PuzzleFinishedEvent
import hu.bme.aut.kutakodj.events.navigation.StuffReadEvent
import hu.bme.aut.kutakodj.logic.enums.ReadableType
import hu.bme.aut.kutakodj.logic.tflite.Classifier
import hu.bme.aut.kutakodj.logic.tflite.ClassifierFloatMobileNet
import hu.bme.aut.kutakodj.navigation.*
import hu.bme.aut.kutakodj.service.AchievementService
import hu.bme.aut.kutakodj.view.fragment.WelcomeFragment
import org.greenrobot.eventbus.EventBus

class MainActivity : BaseActivity(), NavigationOwner {

    var currentReadableType: ReadableType? = null
    private lateinit var achievementService: AchievementService
    private var serviceIsBound: Boolean = false

    private val connection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            val binder = service as AchievementService.LocalBinder
            achievementService = binder.getService()
            serviceIsBound = true
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            serviceIsBound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        addFragment(WelcomeFragment(), R.id.main_content)
    }

    override fun onStart() {
        super.onStart()
        Intent(this, AchievementService::class.java).also { intent ->
            this.bindService(intent, connection, Context.BIND_AUTO_CREATE)
        }
    }

    override fun onStop() {
        if (serviceIsBound) {
            this.unbindService(connection)
            serviceIsBound = false
        }
        super.onStop()
    }

    override fun onReadDataAcquired(data: String) {
        // todo handle model labels to be able to call the event
        if (data.isNotEmpty() && isQR) {
            currentReadableType?.let {
                if (it == ReadableType.PUZZLE) {
                    EventBus.getDefault()
                        .postSticky(PuzzleFinishedEvent(data))
                } else {
                    EventBus.getDefault()
                        .postSticky(StuffReadEvent(data.toLong()))
                }
                onBackPressed()
            }
        }
    }

    override fun onCreateClassifier(): Classifier {
        return ClassifierFloatMobileNet(this, Classifier.Device.CPU, 2)
    }

    override fun onBackPressed() {
        if (backstacks.hasMore()) {
            backPress()
        } else {
            super.onBackPressed()
        }
    }

    override val backstacks: Stack<Pair<Int, Stack<NavBundle>>> = mutableStackOf()
    override var currentFragment: Pair<Int, NavBundle>? = null

    override fun addFragment(
        fragment: Fragment,
        containerId: Int,
        backStackMoveType: BackStackMoveType
    ) {
        currentFragment = when (backStackMoveType) {
            BackStackMoveType.TOP -> {
                val pair = backstacks.popBackStackToTopOf(fragment.javaClass.simpleName)
                pair ?: Pair(containerId, NavBundle(fragment, true))
            }
            BackStackMoveType.NONE -> {
                currentFragment?.let {
                    if (it.second.putOnBackStack) {
                        backstacks.addBackStack(it.second, it.first)
                    }
                }
                Pair(containerId, NavBundle(fragment, true))
            }
        }
        supportFragmentManager
            .beginTransaction()
            .replace(containerId, fragment)
            .commit()
    }

    override fun replaceFragment(fragment: Fragment, containerId: Int) {
        currentFragment?.let {
            if (it.second.putOnBackStack) {
                backstacks.addBackStack(it.second, it.first)
            }
        }
        currentFragment = Pair(containerId, NavBundle(fragment, false))
        supportFragmentManager
            .beginTransaction()
            .replace(containerId, fragment)
            .commit()
    }

    override fun backPress() {
        val bundle = backstacks.popBackStack()
        bundle?.let {
            currentFragment = Pair(it.first, it.second)
            supportFragmentManager
                .beginTransaction()
                .replace(it.first, it.second.fragment)
                .commit()
        }
    }
}
