package hu.bme.aut.kutakodj.logic.enums

enum class ReaderType {
    INACTIVE, NFC, QR, IMAGE
}