package hu.bme.aut.kutakodj.logic.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import hu.bme.aut.kutakodj.data.model.relations.StuffAndTask
import hu.bme.aut.kutakodj.logic.interactors.RoomInteractor
import kotlinx.coroutines.launch

class WellViewModel(
    application: Application,
    private val roomInteractor: RoomInteractor
) : AndroidViewModel(application) {

    private var _currentStuff = MutableLiveData<StuffAndTask>()
    val currentStuff: LiveData<StuffAndTask> = _currentStuff

    private var _wellStuffs = MutableLiveData<List<StuffAndTask>>()
    val wellStuffs: LiveData<List<StuffAndTask>> = _wellStuffs

    fun findStuffById(id: Long) = viewModelScope.launch {
        _currentStuff.postValue(roomInteractor.getStuffById(id))
    }

    fun loadRoomByName(roomName: String) = viewModelScope.launch {
        _wellStuffs.postValue(roomInteractor.getAllItemsOfRoom(roomName))
    }

    fun acquireStuff() = viewModelScope.launch {
        currentStuff.value?.stuff?.let { stuff ->
            roomInteractor.collectStuff(stuff)
            findStuffById(stuff.stuffId)
        }
    }
}
