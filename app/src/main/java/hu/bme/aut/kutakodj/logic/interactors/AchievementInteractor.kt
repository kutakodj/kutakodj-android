package hu.bme.aut.kutakodj.logic.interactors

import android.util.Log
import androidx.lifecycle.LiveData
import hu.bme.aut.kutakodj.data.model.Achievement
import hu.bme.aut.kutakodj.data.model.Rule
import hu.bme.aut.kutakodj.data.model.relations.AchievementWithRules
import hu.bme.aut.kutakodj.data.model.relations.RoomWithRewardsAndStuffs
import hu.bme.aut.kutakodj.data.repository.AchievementRepository
import hu.bme.aut.kutakodj.data.repository.RoomRepository
import hu.bme.aut.kutakodj.data.repository.RuleRepository
import hu.bme.aut.kutakodj.logic.enums.RuleType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AchievementInteractor(
    private val achievementRepository: AchievementRepository,
    private val ruleRepository: RuleRepository,
    private val roomRepository: RoomRepository,
    private val playerInteractor: PlayerInteractor
) {
    val achievements: LiveData<List<AchievementWithRules>> = achievementRepository.allItems

    private suspend fun updateRule(rule: Rule) {
        ruleRepository.update(rule)
    }

    private suspend fun updateAchievement(achievement: Achievement) {
        achievementRepository.update(achievement)
    }

    suspend fun checkRules(shouldBeCheckedRuleType: RuleType? = null) {
        var rules = ruleRepository.getAll()
        val rooms = roomRepository.getAll()
        withContext(Dispatchers.Main) { Log.i("RULE", rules.toString())}
        if (shouldBeCheckedRuleType != null) {
            rules = rules
                .filter { it.ruleType == shouldBeCheckedRuleType }
        }
        rules.forEach { rule ->
            if (!rule.done) handleRuleByType(rule, rooms)
        }
    }

    private suspend fun handleRuleByType(rule: Rule, rooms: List<RoomWithRewardsAndStuffs>) {
        when(rule.ruleType) {
            RuleType.COUNT -> {
                withContext(Dispatchers.Main) {Log.i("RULE", rule.toString())}
                val ruleRoom = rooms.find { room ->
                    room.room.name == rule.countElement
                }
                if (ruleRoom != null && ruleRoom.stuffs.filter { it.isCollected }.size == rule.countMax) {
                    rule.done = true
                    updateRule(rule)
                    // Toast.makeText(this, "Rule done $rule", Toast.LENGTH_LONG).show()
                }
            }
            RuleType.PUZZLE -> {
                rule.done = playerInteractor.hasPlayerFinishedPuzzle()
                updateRule(rule)
            }
            else -> {
                // else is type NONE, this should not happen
            }
        }
    }

    suspend fun checkAchievements() {
        val achievements = achievementRepository.getAll()
        achievements.forEach { achi ->
            if (achi.rules.all { it.done }) {
                // Toast.makeText(this, "Achievement done ${achi.achievement}", Toast.LENGTH_LONG).show()
                achi.achievement.acquired = true
                updateAchievement(achi.achievement)
            }
        }
    }
}