package hu.bme.aut.kutakodj.view.activity

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.hardware.Camera
import android.hardware.camera2.CameraAccessException
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraManager
import android.media.Image
import android.media.ImageReader
import android.nfc.NfcAdapter
import android.os.Handler
import android.os.HandlerThread
import android.os.Trace
import android.util.Log
import android.util.Size
import android.view.Surface
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.gms.vision.barcode.Barcode
import com.google.mlkit.vision.barcode.BarcodeScannerOptions
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.common.InputImage
import hu.bme.aut.kutakodj.Constants
import hu.bme.aut.kutakodj.Constants.BASE_ROTATION
import hu.bme.aut.kutakodj.Constants.isQR
import hu.bme.aut.kutakodj.logic.enums.ReaderType
import hu.bme.aut.kutakodj.logic.enums.ValidatorType
import hu.bme.aut.kutakodj.logic.helpers.ImageHelper
import hu.bme.aut.kutakodj.logic.helpers.NFCHelper
import hu.bme.aut.kutakodj.logic.tflite.Classifier
import hu.bme.aut.kutakodj.logic.tflite.ImageReadConverter
import hu.bme.aut.kutakodj.logic.tflite.Recognition
import hu.bme.aut.kutakodj.util.inputvalidator.InputValidator
import hu.bme.aut.kutakodj.util.inputvalidator.MobileInputValidator
import hu.bme.aut.kutakodj.util.inputvalidator.PuzzleInputValidator
import hu.bme.aut.kutakodj.view.fragment.camera.CameraConnectionFragment
import hu.bme.aut.kutakodj.view.fragment.camera.LegacyCameraConnectionFragment

abstract class BaseActivity : AppCompatActivity(), ImageReader.OnImageAvailableListener,
    Camera.PreviewCallback {

    private var inputValidator: InputValidator = MobileInputValidator()

    private var nfcAdapter: NfcAdapter? = null
    private lateinit var nfcPendingIntent: PendingIntent
    private lateinit var readTagFilters: Array<IntentFilter>

    private var currentReaderType: ReaderType = ReaderType.INACTIVE

    private var classifier: Classifier? = null

    protected var previewWidth = 0
    protected var previewHeight = 0
    private var handler: Handler? = null
    private var handlerThread: HandlerThread? = null
    private var useCamera2API = false
    private var isProcessingFrame = false
    private var yuvBytes = arrayOfNulls<ByteArray>(3)
    private var rgbBytes: IntArray? = null
    private var yRowStride = 0
    private var postInferenceCallback: Runnable? = null
    private var imageConverter: Runnable? = null

    private var rgbFrameBitmap: Bitmap? = null
    private var lastProcessingTimeMs: Long = 0
    private var sensorOrientation: Int = 0

    /** Input image size of the model along x axis.  */
    private var imageSizeX = 0

    /** Input image size of the model along y axis.  */
    private var imageSizeY = 0

    private val qrOptions: BarcodeScannerOptions by lazy {
        BarcodeScannerOptions.Builder()
            .setBarcodeFormats(Barcode.QR_CODE)
            .build()
    }
    val qrScanner = BarcodeScanning.getClient(qrOptions)

    protected open fun getRgbBytes(): IntArray? {
        imageConverter?.run()
        return rgbBytes
    }

    override fun onPreviewFrame(bytes: ByteArray?, camera: Camera?) {
        if (isProcessingFrame) return

        try {
            if (rgbBytes == null) {
                camera?.let {
                    val previewSize: Camera.Size = camera.parameters.previewSize
                    previewHeight = previewSize.height
                    previewWidth = previewSize.width
                    rgbBytes = IntArray(previewWidth * previewHeight)
                    onPreviewSizeChosen(Size(previewSize.width, previewSize.height), 90)
                }
            }
        } catch (e: Exception) {
            Log.e("PreviewFrame", e.stackTrace.toString())
            return
        }

        isProcessingFrame = true
        yuvBytes[0] = bytes
        yRowStride = previewWidth

        imageConverter = Runnable {
            ImageHelper.convertYUV420SPToARGB8888(
                bytes,
                previewWidth,
                previewHeight,
                rgbBytes
            )
        }

        postInferenceCallback = Runnable {
            camera?.addCallbackBuffer(bytes)
            isProcessingFrame = false
        }
        processImage()
    }

    override fun onImageAvailable(reader: ImageReader?) {

        // We need wait until we have some size from onPreviewSizeChosen
        if (previewWidth == 0 || previewHeight == 0) return
        if (rgbBytes == null) {
            rgbBytes = IntArray(previewWidth * previewHeight)
        }

        try {
            val image = reader?.acquireLatestImage() ?: return
            if (isProcessingFrame) {
                image.close()
                return
            }
            isProcessingFrame = true
            Trace.beginSection("imageAvailable")
            val planes = image.planes
            fillBytes(planes, yuvBytes)
            yRowStride = planes[0].rowStride
            val uvRowStride = planes[1].rowStride
            val uvPixelStride = planes[1].pixelStride
            imageConverter = Runnable {
                ImageHelper.convertYUV420ToARGB8888(
                    yuvBytes[0],
                    yuvBytes[1],
                    yuvBytes[2],
                    previewWidth,
                    previewHeight,
                    yRowStride,
                    uvRowStride,
                    uvPixelStride,
                    rgbBytes
                )
            }
            postInferenceCallback = Runnable {
                image.close()
                isProcessingFrame = false
            }
            processImage()
        } catch (e: Exception) {
            Log.e("Image exception!", e.stackTrace.toString())
            Trace.endSection()
            return
        }
        Trace.endSection()
    }

    // Returns true if the device supports the required hardware level, or better.
    private fun isHardwareLevelSupported(
        characteristics: CameraCharacteristics, requiredLevel: Int
    ): Boolean {
        val deviceLevel = characteristics.get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL)!!
        return  if (deviceLevel == CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY)
            requiredLevel == deviceLevel
        else
            requiredLevel <= deviceLevel
    }

    private fun chooseCamera(): String? {
        val manager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            for (cameraId in manager.cameraIdList) {
                val characteristics = manager.getCameraCharacteristics(cameraId)

                // We don't use a front facing camera in this project.
                val facing = characteristics.get(CameraCharacteristics.LENS_FACING)
                if (facing != null && facing == CameraCharacteristics.LENS_FACING_FRONT) continue

                val map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)
                    ?: continue

                // Fallback to camera1 API for internal cameras that don't have full support.
                // This should help with legacy situations where using the camera2 API causes
                // distorted or otherwise broken previews.
                useCamera2API = (facing == CameraCharacteristics.LENS_FACING_EXTERNAL
                        || isHardwareLevelSupported(
                    characteristics, CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_FULL
                ))
                Log.i("CAMERA_API", "Camera API lv2?: $useCamera2API")
                return cameraId
            }
        } catch (e: CameraAccessException) {
            Log.e("No access camera", e.stackTrace.toString())
        }
        return null
    }

    protected open fun fillBytes(
        planes: Array<Image.Plane>,
        yuvBytes: Array<ByteArray?>
    ) {
        // Because of the variable row stride it's not possible to know in
        // advance the actual necessary dimensions of the yuv planes.
        for (i in planes.indices) {
            val buffer = planes[i].buffer
            if (yuvBytes[i] == null) {
                Log.d("BUFFER", "Initializing buffer $i at size ${buffer.capacity()}")
                yuvBytes[i] = ByteArray(buffer.capacity())
            }
            yuvBytes[i]?.let {
                buffer[it]
            }
        }
    }

    protected open fun readyForNextImage() {
        postInferenceCallback?.let {
            it.run()
        }
    }

    protected open fun getScreenOrientation(): Int {
        return when (windowManager.defaultDisplay.rotation) {
            Surface.ROTATION_270 -> 270
            Surface.ROTATION_180 -> 180
            Surface.ROTATION_90 -> 90
            else -> 0
        }
    }

    @Synchronized
    protected open fun runInBackground(runnable: Runnable?) {
        if (handler != null && runnable != null) {
            handler?.post(runnable)
        }
    }

    fun getCameraFragment(): Fragment? {
        val cameraId = chooseCamera()
        return if (useCamera2API) {
           CameraConnectionFragment.newInstance(
                object : CameraConnectionFragment.ConnectionCallback {
                    override fun onPreviewSizeChosen(
                        size: Size,
                        cameraRotation: Int
                    ) {
                        previewHeight = size.height
                        previewWidth = size.width
                        onPreviewSizeChosen(size, cameraRotation)
                    }
                },
                this,
                getDesiredPreviewFrameSize()
            ).apply {
               cameraId?.let {
                   this.setCamera(it)
               }
           }
        } else {
            LegacyCameraConnectionFragment.newInstance(this, getDesiredPreviewFrameSize())
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)

        if (intent.action != null && NfcAdapter.ACTION_NDEF_DISCOVERED == intent.action) {
            val rawdata = NFCHelper.processNFCData(intent)
            val data = inputValidator.validateInput(rawdata)
            Log.i("RESULT", "$data")
            onReadDataAcquired(data)
        }
    }

    override fun onResume() {
        super.onResume()
        startReading()
    }

    override fun onPause() {
        stopReading()
        super.onPause()
    }

    private fun initNfc() {
        nfcPendingIntent = PendingIntent.getActivity(
            this,
            0,
            Intent(this, javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),
            0
        )

        val ndefDetected = IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED)
        try {
            // ndefDetected.addDataType("application/hu.bme.aut.kutakodj.logic.nfc")
            ndefDetected.addDataType("text/plain")
        } catch (e: IntentFilter.MalformedMimeTypeException) {
            throw RuntimeException("Could not add MIME type.", e)
        }

        readTagFilters = arrayOf(ndefDetected)
    }

    private fun startCamera() {
        handlerThread = HandlerThread("inference")
        handlerThread?.let { innerHandlerThread ->
            innerHandlerThread.start()
            handler = innerHandlerThread.looper?.let { Handler(it) }
        }

    }

    private fun stopCamera() {
        handlerThread?.let {
            it.quitSafely()
            try {
                it.join()
                handlerThread = null
                handler = null
            } catch (e: Exception) {
                Log.e("Exception", e.stackTrace.toString())
            }
        }
    }

    /*
    Changes current inputvalidator
    Should be changed when the puzzle code needs to be read
     */
    private fun changeValidator(type: ValidatorType) {
        inputValidator = when (type) {
            ValidatorType.PUZZLE -> PuzzleInputValidator()
            ValidatorType.MOBILE -> MobileInputValidator()
        }
    }

    /*
    Information reading can be enabled with this
    Selecting the form of reading is automatic
     */
    fun activateReading(requestedReadingType: ReaderType, validatorType: ValidatorType = ValidatorType.MOBILE) {
        changeValidator(validatorType)
        setReadingTypeOnActive(requestedReadingType)
        startReading()
    }

    /*
    Information reading can be disabled with this
     */
    fun deactivateReading() {
        stopReading()
        currentReaderType = ReaderType.INACTIVE
    }

    fun getAvailableReadingType(): ReaderType {
        nfcAdapter = NfcAdapter.getDefaultAdapter(this)
        return if (nfcAdapter != null && nfcAdapter?.isEnabled == true) {
            ReaderType.NFC
        } else if (isQR) {
            ReaderType.QR
        } else {
            ReaderType.IMAGE
        }
    }

    private fun setReadingTypeOnActive(requestedReadingType: ReaderType) {
        nfcAdapter = NfcAdapter.getDefaultAdapter(this)

        if (nfcAdapter != null && nfcAdapter?.isEnabled == true && inputValidator !is PuzzleInputValidator) {
            currentReaderType = ReaderType.NFC
            initNfc()
        } else if (requestedReadingType == ReaderType.QR) {
            Toast.makeText(this, "Your device does not support NFC.", Toast.LENGTH_LONG).show()
            currentReaderType = ReaderType.QR
        } else if (requestedReadingType == ReaderType.IMAGE) {
            if (classifier == null) classifier = onCreateClassifier()
            currentReaderType = ReaderType.IMAGE
        }
    }

    private fun startReading() {
        when (currentReaderType) {
            ReaderType.NFC -> nfcAdapter?.enableForegroundDispatch(
                this,
                nfcPendingIntent,
                readTagFilters,
                null
            )
            ReaderType.QR, ReaderType.IMAGE -> startCamera()
        }
    }

    private fun stopReading() {
        when (currentReaderType) {
            ReaderType.NFC -> nfcAdapter?.disableForegroundDispatch(this)
            ReaderType.QR, ReaderType.IMAGE -> stopCamera() // Stop camera on pause
        }
    }

    /*
    Extending classes should override this function and handle the data read from the nfc or camera input
     */
    abstract fun onReadDataAcquired(data: String)
    abstract fun onCreateClassifier(): Classifier

    private fun processImage() {
        rgbFrameBitmap?.setPixels(
            getRgbBytes(),
            0,
            previewWidth,
            0,
            0,
            previewWidth,
            previewHeight
        )

        runInBackground(
            Runnable {
                when (currentReaderType) {
                    ReaderType.QR -> {
                        readQr()
                    }
                    ReaderType.IMAGE -> {
                        val recognition = classifyImage()
                        runOnUiThread {
                            onReadDataAcquired(ImageReadConverter.convertLabelToId(recognition?.title ?: ""))
                        }
                    }
                    else -> {
                    }
                }
                readyForNextImage()
            })
    }

    private fun onPreviewSizeChosen(size: Size, rotation: Int) {
        previewWidth = size.width
        previewHeight = size.height
        sensorOrientation = rotation - getScreenOrientation()
        Log.i("CAMERA", "Camera orientation relative to screen canvas: $sensorOrientation")
        Log.i("CAMERA", "Initializing at size $previewWidth x $previewHeight")
        rgbFrameBitmap = Bitmap.createBitmap(previewWidth, previewHeight, Bitmap.Config.ARGB_8888)
    }

    protected fun getDesiredPreviewFrameSize(): Size = Constants.DESIRED_PREVIEW_SIZE

    private fun classifyImage(): Recognition? {
        if (rgbFrameBitmap == null) return null
        val sensorOrientation = BASE_ROTATION - getScreenOrientation()
        val results: List<Recognition?>? =
            classifier?.recognizeImage(rgbFrameBitmap, sensorOrientation)
        Log.v("DETECTING", "Detect: $results")
        // todo add a threshold
        return results?.filterNotNull()?.maxByOrNull { it.confidence }
    }

    private fun readQr() {
        rgbFrameBitmap?.let { bitmap ->
            val image = InputImage.fromBitmap(bitmap, 0)
            qrScanner.process(image)
                .addOnSuccessListener { barcodes ->
                    // there may be multiple barcodes,
                    // we gonna only send back the first
                    val results = barcodes.map {
                        inputValidator.validateInput(barcodes[0].rawValue)
                    }
                    val result = results.firstOrNull { it.isNotEmpty() }
                    onReadDataAcquired(result ?: "")
                }
                .addOnFailureListener {
                    Log.e("QR Reader", "Exception while reading qr code")
                    it.printStackTrace()
                }
        }
    }
}