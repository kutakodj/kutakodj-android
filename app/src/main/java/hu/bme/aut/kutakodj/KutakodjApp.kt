package hu.bme.aut.kutakodj

import android.app.Application
import hu.bme.aut.kutakodj.data.dataModule
import hu.bme.aut.kutakodj.logic.logicModule
import org.greenrobot.eventbus.EventBus
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class KutakodjApp : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@KutakodjApp)
            modules(listOf(dataModule, logicModule))
        }
        EventBus.builder().throwSubscriberException(BuildConfig.DEBUG).installDefaultEventBus()
    }
}