package hu.bme.aut.kutakodj.view.viewholder

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textview.MaterialTextView
import com.mikhaellopez.circularimageview.CircularImageView
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.data.model.Answer
import hu.bme.aut.kutakodj.util.getCharFromPosition

class AnswerPictureViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val civ_answerPicture: CircularImageView = itemView.findViewById(R.id.civ_answer_picture)
    val tv_answerCount: MaterialTextView = itemView.findViewById(R.id.tv_answer_count_pic)
    var item: Answer? = null

    fun setPictureAnswerDetails(item: Answer, position: Int, context: Context) {
        tv_answerCount.text = getCharFromPosition(position)
        this.item = item
        val id = context.resources.getIdentifier(item.answerText, "drawable", context.packageName)
        civ_answerPicture.setImageResource(id)
    }
}
