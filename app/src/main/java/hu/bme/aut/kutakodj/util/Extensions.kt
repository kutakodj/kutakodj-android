package hu.bme.aut.kutakodj.util

import android.app.Activity
import android.view.Window
import android.view.WindowManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

fun Activity.switchStatusBarColor(colorId: Int) {
    val window: Window = this.window
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    val color = resources.getColor(colorId)
    if (window.statusBarColor != color) window.statusBarColor = color
}

fun <K, V> lazyMap(initializer: (K) -> V): Map<K, V> {
    val map = mutableMapOf<K, V>()
    return map.withDefault { key ->
        val newValue = initializer(key)
        map[key] = newValue
        return@withDefault newValue
    }
}

fun getCharFromPosition(position: Int): String {
    return when (position) {
        0 -> "A"
        1 -> "B"
        2 -> "C"
        else -> "X"
    }
}

fun <T, K, R> LiveData<T>.combineWith(
    liveData: LiveData<K>,
    block: (T?, K?) -> R
): LiveData<R> {
    val result = MediatorLiveData<R>()
    result.addSource(this) {
        result.value = block.invoke(this.value, liveData.value)
    }
    result.addSource(liveData) {
        result.value = block.invoke(this.value, liveData.value)
    }
    return result
}

fun Int.toBoolean(): Boolean = this == 1
