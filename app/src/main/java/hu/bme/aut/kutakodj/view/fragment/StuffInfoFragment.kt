package hu.bme.aut.kutakodj.view.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.data.model.Stuff
import hu.bme.aut.kutakodj.events.navigation.StuffRequestedEvent
import hu.bme.aut.kutakodj.logic.viewmodels.WellViewModel
import kotlinx.android.synthetic.main.fragment_stuff_info.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.koin.androidx.viewmodel.ext.android.getViewModel

class StuffInfoFragment : Fragment(R.layout.fragment_stuff_info) {

    private lateinit var wellViewModel: WellViewModel

    companion object {
        const val KEY_STUFF_ID = "stuff_id"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        frame_back.setOnClickListener {
            requireActivity().onBackPressed()
        }

        wellViewModel = getViewModel()
        arguments?.getLong(KEY_STUFF_ID)?.let { stuffId ->
            wellViewModel.findStuffById(stuffId)
            wellViewModel.currentStuff.observe(viewLifecycleOwner, Observer { liveData ->
                initData(liveData.stuff)
            })
        }
    }

    private fun initData(stuff: Stuff) {
        if (stuff.isCollected) {
            tv_object_name.text = stuff.name
            tv_object_description.text = stuff.description
            civ_object_picture.setImageResource(resources.getIdentifier(stuff.picture, "drawable", context?.packageName))
        } else {
            tv_object_name.text = "????"
            tv_object_description.text = "????"
            civ_object_picture.setImageResource(resources.getIdentifier("question_mark", "drawable", context?.packageName))
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED, sticky = true)
    open fun onStuffRequestedEvent(event: StuffRequestedEvent) {
        val stickyEvent = EventBus.getDefault().removeStickyEvent(StuffRequestedEvent::class.java)
        if (stickyEvent != null) {
            wellViewModel.findStuffById(event.stuffId)
            wellViewModel.currentStuff.observe(viewLifecycleOwner, Observer { stuff ->
                wellViewModel.acquireStuff()
                initData(stuff.stuff)
            })
        }
    }
}
