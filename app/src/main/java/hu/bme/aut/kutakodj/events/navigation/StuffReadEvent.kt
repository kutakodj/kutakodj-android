package hu.bme.aut.kutakodj.events.navigation

data class StuffReadEvent(val stuffId: Long)
