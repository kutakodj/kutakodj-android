package hu.bme.aut.kutakodj.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "answer_table")
data class Answer(
    @PrimaryKey(autoGenerate = true) val answerId: Long,
    val questionOwnerId: Long,
    val answerText: String
)
