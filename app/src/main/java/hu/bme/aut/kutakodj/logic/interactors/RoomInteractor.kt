package hu.bme.aut.kutakodj.logic.interactors

import hu.bme.aut.kutakodj.data.model.Stuff
import hu.bme.aut.kutakodj.data.model.relations.StuffAndTask
import hu.bme.aut.kutakodj.data.repository.RoomRepository
import hu.bme.aut.kutakodj.data.repository.StuffRepository

class RoomInteractor(
    private val roomRepository: RoomRepository,
    private val stuffRepository: StuffRepository
) {

    suspend fun getAllItemsOfRoom(roomName: String?): List<StuffAndTask> =
        if (roomName == null) {
            stuffRepository.getAll()
        } else {
            val room = roomRepository.getByName(roomName)
            stuffRepository.getByRoomId(room.room.roomId)
        }

    suspend fun getStuffById(stuffId: Long) = stuffRepository.getById(stuffId)

    suspend fun getRoomById(roomId: Long) = roomRepository.getById(roomId)

    suspend fun collectStuff(stuff: Stuff) {
        stuff.isCollected = true
        stuffRepository.update(stuff)
    }
}