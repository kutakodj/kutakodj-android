package hu.bme.aut.kutakodj.logic.enums

enum class RuleType(val value: String) {
    COUNT("count"), PUZZLE("puzzle"), NONE("undefined")
}

fun String.toRuleType(): RuleType {
    return RuleType.values().find { it.value == this } ?: RuleType.NONE
}