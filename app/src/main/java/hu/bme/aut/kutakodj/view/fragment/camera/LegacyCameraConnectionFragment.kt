package hu.bme.aut.kutakodj.view.fragment.camera

import android.graphics.SurfaceTexture
import android.hardware.Camera
import android.os.HandlerThread
import android.util.Log
import android.util.Size
import android.view.TextureView
import androidx.fragment.app.Fragment
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.logic.helpers.CameraHelper
import hu.bme.aut.kutakodj.logic.helpers.ImageHelper
import kotlinx.android.synthetic.main.fragment_camera.*

// This class is deliberately uses deprecated api, its purpose is to handle that...
class LegacyCameraConnectionFragment private constructor(
    private val imageListener: Camera.PreviewCallback,
    private val desiredSize: Size
) : Fragment(R.layout.fragment_camera) {

    companion object {
        fun newInstance(
            imageListener: Camera.PreviewCallback,
            desiredSize: Size
        ): LegacyCameraConnectionFragment {
            return LegacyCameraConnectionFragment(imageListener, desiredSize)
        }
    }

    private var camera: Camera? = null
    private var backgroundThread: HandlerThread? = null

    private val surfaceTextureListener = object : TextureView.SurfaceTextureListener {
        override fun onSurfaceTextureSizeChanged(
            surfaceTexture: SurfaceTexture,
            width: Int,
            height: Int
        ) {
        }

        override fun onSurfaceTextureUpdated(surfaceTexture: SurfaceTexture) {}

        override fun onSurfaceTextureDestroyed(surfaceTexture: SurfaceTexture): Boolean = true

        override fun onSurfaceTextureAvailable(
            surfaceTexture: SurfaceTexture,
            width: Int,
            height: Int
        ) {
            val index = getCameraId()
            try {
                camera = Camera.open(index)
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }


            try {
                val immutableCamera = camera
                immutableCamera?.let { camera ->
                    val parameters = camera.parameters
                    val focusModes = camera.parameters.supportedFocusModes
                    if (focusModes?.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE) == true) {
                        parameters.focusMode =
                            Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE
                    }
                    val cameraSizes = camera.parameters.supportedPreviewSizes.map {
                        Size(it.width, it.height)
                    }
                    val previewSize = CameraHelper.chooseOptimalSize(
                        cameraSizes,
                        desiredSize.width,
                        desiredSize.height
                    )
                    previewSize?.let {
                        parameters.setPreviewSize(it.width, it.height)
                    }
                    camera.parameters = parameters
                    camera.setDisplayOrientation(90)
                    camera.setPreviewTexture(surfaceTexture)
                }
            } catch (e: Exception) {
                camera?.release()
            }
            camera?.setPreviewCallbackWithBuffer(imageListener)
            val cameraSize = camera?.parameters?.previewSize
            cameraSize?.let {
                camera?.addCallbackBuffer(
                    ByteArray(
                        ImageHelper.getYUVByteSize(
                            it.height,
                            it.width
                        )
                    )
                )
                texture.setAspectRatio(it.height, it.width)
            }
            camera?.startPreview()
        }
    }

    override fun onResume() {
        super.onResume()
        startBackgroundThread()
        // When the screen is turned off and turned back on, the SurfaceTexture is already
        // available, and "onSurfaceTextureAvailable" will not be called. In that case, we can open
        // a camera and start preview from here (otherwise, we wait until the surface is ready in
        // the SurfaceTextureListener).

        if (texture.isAvailable) {
            camera?.startPreview()
        } else {
            texture.surfaceTextureListener = surfaceTextureListener
        }
    }

    override fun onPause() {
        stopCamera()
        stopBackgroundThread()
        super.onPause()
    }

    /** Starts a background thread and its {@link Handler}. */
    private fun startBackgroundThread() {
        backgroundThread = HandlerThread("CameraBackground")
        backgroundThread?.start()
    }

    /** Stops the background thread and its {@link Handler}. */
    private fun stopBackgroundThread() {
        backgroundThread?.quitSafely()
        try {
            backgroundThread?.join()
            backgroundThread = null
        } catch (e: InterruptedException) {
            Log.e("BGTHREAD", "Stopping bgthread failed in LegacyConnectionFragment!")
            e.printStackTrace()
        }
    }

    protected fun stopCamera() {
        camera?.apply {
            stopPreview()
            setPreviewCallback(null)
            release()
            camera = null
        }
    }

    private fun getCameraId(): Int {
        val cameraInfo = Camera.CameraInfo()
        for (i in 0 until Camera.getNumberOfCameras()) {
            Camera.getCameraInfo(i, cameraInfo)
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) return i
        }
        // No camera was found
        return -1
    }
}