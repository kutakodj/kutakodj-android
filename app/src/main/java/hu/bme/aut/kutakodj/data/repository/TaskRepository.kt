package hu.bme.aut.kutakodj.data.repository

import hu.bme.aut.kutakodj.data.dao.TaskDao
import hu.bme.aut.kutakodj.data.model.Task
import hu.bme.aut.kutakodj.data.model.relations.TaskWithQuestions

class TaskRepository(private val dao: TaskDao) {

    suspend fun getAll() = dao.getAll()

    suspend fun getById(id: Long): TaskWithQuestions = dao.getById(id)

    suspend fun insertAll(vararg obj: Task) = dao.insertAll(*obj)

    suspend fun insert(obj: Task) = dao.insert(obj)

    suspend fun update(obj: Task) = dao.update(obj)

    suspend fun delete(obj: Task) = dao.delete(obj)
}
