package hu.bme.aut.kutakodj.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import hu.bme.aut.kutakodj.data.model.Reward

@Dao
interface RewardDao : BaseDao<Reward> {

    @Transaction
    @Query("Select * from reward_table")
    fun getAll(): LiveData<List<Reward>>

    @Transaction
    @Query("Select * from reward_table where rewardId = (:rewardId)")
    fun getById(rewardId: Long): LiveData<Reward>

    @Transaction
    @Query("Delete from reward_table")
    suspend fun deleteAll()
}
