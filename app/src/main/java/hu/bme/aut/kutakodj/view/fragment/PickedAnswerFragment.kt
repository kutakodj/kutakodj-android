package hu.bme.aut.kutakodj.view.fragment

import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.DrawableImageViewTarget
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.logic.viewmodels.QuestionViewModel
import hu.bme.aut.kutakodj.navigation.getNavOwner
import kotlinx.android.synthetic.main.fragment_picked_answer.*
import org.koin.androidx.viewmodel.ext.android.getSharedViewModel
import kotlin.random.Random

class PickedAnswerFragment : Fragment(R.layout.fragment_picked_answer) {

    private lateinit var questionViewModel: QuestionViewModel
    private val randomGenerator = Random.Default

    val timer = object : CountDownTimer(3000, 1000) {
        override fun onTick(millisUntilFinished: Long) {
            tv_timer?.let {
                it.text = ((millisUntilFinished / 1000)).toString()
            }
        }

        override fun onFinish() {
            activity?.let {
                it.getNavOwner()
                ?.replaceFragment(QuestionFragment().apply {
                    arguments = Bundle().apply {
                        putLong(QuestionFragment.KEY_STUFF_ID, questionViewModel.currentStuff.value!!.stuff.stuffId)
                    }
                }, R.id.fragment_container_quiz)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        questionViewModel = getSharedViewModel()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (questionViewModel.currentQuestion.value?.question?.correctAnswerId == questionViewModel.pickedAnswerId) {
            tv_picked_answer_title.text = resources.getString(R.string.correct_answer)
            tv_picked_answer_title.setTextColor(resources.getColor(R.color.textDarkOK))
            tv_answer_reaction.visibility = View.GONE
            tv_timer.visibility = View.GONE
            civ_congrat.visibility = View.VISIBLE

            val target = DrawableImageViewTarget(civ_congrat)
            Glide
                .with(this)
                .load(selectGifOrRandom())
                .into(target)

            questionViewModel.tryCount = 3
            questionViewModel.acquireStuff()
            //questionViewModel.resetPool()
        } else {

            if (questionViewModel.tryCount == 0) {
                tv_answer_reaction.text = resources.getString(R.string.incorrect_answer_0_try)
                tv_timer.visibility = View.GONE
                questionViewModel.tryCount = 3
            } else {
                tv_answer_reaction.text = resources.getString(R.string.incorrect_answer_try_again)
                tv_timer.visibility = View.VISIBLE
                timer.start()
            }

            tv_picked_answer_title.text = resources.getString(R.string.incorrect_answer)
            tv_picked_answer_title.setTextColor(resources.getColor(R.color.textDarkError))
            tv_answer_reaction.visibility = View.VISIBLE
            civ_congrat.visibility = View.GONE
        }

        questionViewModel.stationNumber.observe(viewLifecycleOwner, Observer {
            tv_station_name.text = getString(R.string.question_station_name, it)
        })

        frame_back.setOnClickListener {
            questionViewModel.tryCount = 3
            requireActivity().onBackPressed()
        }
    }

    private fun selectGifOrRandom(): Int {
        val number = if (questionViewModel.currentStuff.value?.stuff?.stuffId != null) {
            questionViewModel.currentStuff.value?.stuff?.stuffId.toString().padStart(2, '0')
        } else {
            randomGenerator.nextInt(1, 12).toString().padStart(2, '0')
        }
        return resources.getIdentifier("dimu_app_kaptal_egy_karpitot_$number", "raw", requireContext().packageName)
    }
}
