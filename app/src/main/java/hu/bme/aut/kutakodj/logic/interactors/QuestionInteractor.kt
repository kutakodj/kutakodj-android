package hu.bme.aut.kutakodj.logic.interactors

import hu.bme.aut.kutakodj.data.model.relations.QuestionWithAnswers
import hu.bme.aut.kutakodj.data.repository.QuestionRepository
import hu.bme.aut.kutakodj.data.repository.TaskRepository
import kotlin.random.Random

class QuestionInteractor(
    private val questionRepository: QuestionRepository,
    private val taskRepository: TaskRepository
) {

    private var questionPool: MutableList<QuestionWithAnswers> = mutableListOf()
    private var currentTaskId: Long = 0

    suspend fun loadQuestionsOfTask(taskId: Long) {
        currentTaskId = taskId
        questionPool.clear()
        val currentTask = taskRepository.getById(taskId)
        currentTask.questions.forEach { taskQuestion ->
            val question = questionRepository.getById(taskQuestion.questionId)
            questionPool.add(question)
        }
    }

    suspend fun getNextQuestionFromPool(taskId: Long): QuestionWithAnswers? {
        if (questionPool.isNullOrEmpty()) loadQuestionsOfTask(taskId)
        return if (!questionPool.isNullOrEmpty()) {
            if (questionPool.size == 1)
                questionPool.removeAt(0)
            else
                questionPool.removeAt(Random.nextInt(0, questionPool.size - 1))
        } else {
            loadQuestionsOfTask(currentTaskId)
            null
        }
    }

}