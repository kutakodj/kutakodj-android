package hu.bme.aut.kutakodj.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "task_table")
open class Task(
    @PrimaryKey(autoGenerate = true) val taskId: Long,
    val stuffOwnerId: Long,
    val taskType: String
)
