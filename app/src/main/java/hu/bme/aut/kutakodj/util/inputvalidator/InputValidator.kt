package hu.bme.aut.kutakodj.util.inputvalidator

interface InputValidator {
    fun validateInput(input: String?): String
}
