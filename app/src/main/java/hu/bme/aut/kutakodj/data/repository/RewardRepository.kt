package hu.bme.aut.kutakodj.data.repository

import androidx.lifecycle.LiveData
import hu.bme.aut.kutakodj.data.dao.RewardDao
import hu.bme.aut.kutakodj.data.model.Reward

class RewardRepository(private val dao: RewardDao) {
    val allItems: LiveData<List<Reward>> = dao.getAll()

    fun getById(id: Long): LiveData<Reward> = dao.getById(id)

    suspend fun insertAll(vararg obj: Reward) = dao.insertAll(*obj)

    suspend fun insert(obj: Reward) = dao.insert(obj)

    suspend fun update(obj: Reward) = dao.update(obj)

    suspend fun delete(obj: Reward) = dao.delete(obj)
}
