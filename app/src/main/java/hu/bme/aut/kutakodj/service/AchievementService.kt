package hu.bme.aut.kutakodj.service

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.LiveData
import androidx.lifecycle.lifecycleScope
import hu.bme.aut.kutakodj.data.model.relations.AchievementWithRules
import hu.bme.aut.kutakodj.events.signals.RuleSignalEvent
import hu.bme.aut.kutakodj.logic.enums.toRuleType
import hu.bme.aut.kutakodj.logic.interactors.AchievementInteractor
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.koin.android.ext.android.inject

class AchievementService : LifecycleService() {
    private val achievementInteractor: AchievementInteractor by inject()

    private val binder = LocalBinder()

    val achievements: LiveData<List<AchievementWithRules>>

    init {
        achievements = achievementInteractor.achievements
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)

        return Service.START_REDELIVER_INTENT
    }

    override fun onBind(intent: Intent): IBinder? {
        super.onBind(intent)
        lifecycleScope.launchWhenResumed {
            achievementInteractor.checkRules()
            achievementInteractor.checkAchievements()
        }

        return binder
    }

    override fun onCreate() {
        super.onCreate()
        EventBus.getDefault().register(this)
        lifecycleScope.launchWhenCreated {
            achievementInteractor.checkRules()
            achievementInteractor.checkAchievements()
        }
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    inner class LocalBinder : Binder() {
        fun getService(): AchievementService = this@AchievementService
    }

    @Subscribe(threadMode = ThreadMode.ASYNC, sticky = true)
    fun onRuleSignalEvent(event: RuleSignalEvent) = lifecycleScope.launch {
        val stickyEvent = EventBus.getDefault().removeStickyEvent(RuleSignalEvent::class.java)
        if (stickyEvent != null) {
            achievementInteractor.checkRules(stickyEvent.type.toRuleType())
        }
        achievementInteractor.checkAchievements()
    }
}
