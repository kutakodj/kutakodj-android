package hu.bme.aut.kutakodj.logic.interactors

import hu.bme.aut.kutakodj.data.model.Player
import hu.bme.aut.kutakodj.data.repository.PlayerRepository
import hu.bme.aut.kutakodj.util.CodeGenerator

class PlayerInteractor(private val playerRepository: PlayerRepository) {
    private val codeGenerator = CodeGenerator()
    private val ACTIVE_PLAYER_ID: Long = 1

    suspend fun assignCodeToPlayer() {
        val player = getActivePlayer()
        if (player.player.code.isEmpty()) {
            player.player.code = codeGenerator.generateCodeForPlayer()
            updatePlayer(player.player)
        }
    }

    suspend fun checkPlayerCode(code: String) {
        val player = getActivePlayer()
        if (code == player.player.code) {
            player.player.puzzleOK = true
            updatePlayer(player.player)
        }
    }

    suspend fun hasPlayerFinishedPuzzle(): Boolean {
        val player = getActivePlayer()
        return player.player.puzzleOK
    }

    suspend fun getActivePlayer() = playerRepository.getById(ACTIVE_PLAYER_ID)

    private suspend fun updatePlayer(player: Player) = playerRepository.update(player)
}