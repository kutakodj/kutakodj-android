package hu.bme.aut.kutakodj.data.model.relations

import androidx.room.Embedded
import androidx.room.Relation
import hu.bme.aut.kutakodj.data.model.Player
import hu.bme.aut.kutakodj.data.model.Room

data class PlayerWithRooms(
    @Embedded val player: Player,
    @Relation(
        parentColumn = "playerId",
        entityColumn = "playerOwnerId"
    )
    val rooms: List<Room>
)
