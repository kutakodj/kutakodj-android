package hu.bme.aut.kutakodj.data.repository

import hu.bme.aut.kutakodj.data.dao.StuffDao
import hu.bme.aut.kutakodj.data.model.Stuff
import hu.bme.aut.kutakodj.data.model.relations.StuffAndTask

class StuffRepository(private val dao: StuffDao) {

    suspend fun getAll(): List<StuffAndTask> = dao.getAll()

    suspend fun getById(id: Long): StuffAndTask = dao.getById(id)

    suspend fun getByRoomId(id: Long): List<StuffAndTask> = dao.getByRoomId(id)

    suspend fun insertAll(vararg obj: Stuff) = dao.insertAll(*obj)

    suspend fun insert(obj: Stuff) = dao.insert(obj)

    suspend fun update(obj: Stuff) = dao.update(obj)

    suspend fun delete(obj: Stuff) = dao.delete(obj)
}
