package hu.bme.aut.kutakodj.events.navigation

data class TaskRequestedEvent(val stuffId: Long, val taskType: String)
