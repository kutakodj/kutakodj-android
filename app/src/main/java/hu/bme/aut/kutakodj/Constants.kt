package hu.bme.aut.kutakodj

import android.util.Size
import android.util.SparseIntArray
import android.view.Surface

object Constants {
    // Model based on edge detection?
    const val PREPROCESS_EDGES = false

    // ImageHelper
    // This value is 2 ^ 18 - 1, and is used to clamp the RGB values before their ranges
    // are normalized to eight bits.
    const val K_MAX_CHANNEL_VALUE = 262143

    const val TEXT_SIZE_DIP = 16F

    // Classifier
    const val MAX_RESULTS = 3

    // Float MobileNet
    // For additional normalization of the input
    const val IMAGE_MEAN = 0f
    const val IMAGE_STD = 255f

    // Float model does not need dequantization in the post-processing. Setting mean and std as 0.0f
    // and 1.0f, repectively, to bypass the normalization.
    const val PROBABILITY_MEAN = 0.0f
    const val PROBABILITY_STD = 1.0f

    // Main
    val DESIRED_PREVIEW_SIZE = Size(640, 480)
    const val MAIN_TEXT_SIZE_DIP = 10F

    // CameraFragment
    /**
     * The camera preview size will be chosen to be the smallest frame by pixel size capable of
     * containing a DESIRED_SIZE x DESIRED_SIZE square.
     */
    const val MINIMUM_PREVIEW_SIZE = 320

    val ORIENTATIONS = SparseIntArray().apply {
        append(Surface.ROTATION_0, 90)
        append(Surface.ROTATION_90, 0)
        append(Surface.ROTATION_180, 270)
        append(Surface.ROTATION_270, 180)
    }

    const val BASE_ROTATION = 90

    // QR vagy Img recog on camera use
    const val isQR = true
}