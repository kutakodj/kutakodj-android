package hu.bme.aut.kutakodj.events.signals

data class RuleSignalEvent(val type: String)
