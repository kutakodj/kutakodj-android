package hu.bme.aut.kutakodj.logic.helpers

import android.text.TextUtils
import android.util.Log
import android.util.Size
import hu.bme.aut.kutakodj.Constants
import kotlin.math.sign

object CameraHelper {
    fun chooseOptimalSize(choices: List<Size>, width: Int, height: Int): Size? {
        val minSize = width.coerceAtMost(height).coerceAtLeast(Constants.MINIMUM_PREVIEW_SIZE)
        val desiredSize = Size(width, height)

        // Collect the supported resolutions that are at least as big as the preview Surface
        val exactSizeFound = choices.any { it == desiredSize }
        val bigEnough: List<Size> = choices.filter { it.height >= minSize && it.width >= minSize }
        val tooSmall: List<Size> = choices.filter { it.height < minSize || it.width < minSize }

        Log.d("PREVIEW SIZE", "Desired size: $desiredSize, min size: $minSize, x: $minSize")
        Log.d("PREVIEW SIZE", "Valid preview sizes: [\" + ${TextUtils.join(", ", bigEnough)}  \"]")
        Log.d(
            "PREVIEW SIZE",
            "Rejected preview sizes: [\" + ${TextUtils.join(", ", tooSmall)}  \"]"
        )

        if (exactSizeFound) {
            Log.d("PREVIEW SIZE", "Exact size match found.")
            return desiredSize
        }
        if (bigEnough.isNotEmpty()) {
            val chosenSize = bigEnough.minWithOrNull(kotlin.Comparator { lhs, rhs ->
                return@Comparator (
                        lhs.width.toLong() * lhs.height.toLong() -
                                rhs.width.toLong() * rhs.height.toLong()).sign
            })
            Log.d("PREVIEW SIZE", "Chosen size: ${chosenSize?.width} x ${chosenSize?.height}")
            chosenSize?.let { return chosenSize }
        }
        Log.d("PREVIEW SIZE", "Couldn't find any suitable preview size")

        return null
    }
}