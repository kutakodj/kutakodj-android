package hu.bme.aut.kutakodj.navigation

enum class BackStackMoveType {
    TOP, NONE
}