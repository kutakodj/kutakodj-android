package hu.bme.aut.kutakodj.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.DrawableImageViewTarget
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.events.navigation.PuzzleFinishedEvent
import hu.bme.aut.kutakodj.events.signals.RuleSignalEvent
import hu.bme.aut.kutakodj.logic.enums.ReadableType
import hu.bme.aut.kutakodj.logic.viewmodels.FinishViewModel
import hu.bme.aut.kutakodj.navigation.getNavOwner
import jp.wasabeef.glide.transformations.CropCircleWithBorderTransformation
import kotlinx.android.synthetic.main.fragment_finish.*
import kotlinx.android.synthetic.main.fragment_puzzle.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.koin.androidx.viewmodel.ext.android.getViewModel

class FinishFragment : Fragment() {

    private lateinit var finishViewModel: FinishViewModel

    companion object {
        const val STATE_KEY = "state"
        enum class FinishState(val value: String) {
            FINISH("finish"), PUZZLE("puzzle")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        finishViewModel = getViewModel()

        finishViewModel.assignCodeToPlayer()

        return when (arguments?.getString(STATE_KEY)) {
            FinishState.FINISH.value -> {
                inflater.inflate(R.layout.fragment_finish, container, false)
            }
            else -> {
                inflater.inflate(R.layout.fragment_puzzle, container, false)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when (arguments?.getString(STATE_KEY)) {
            FinishState.FINISH.value -> {
                initFinishView()
            }
            else -> {
                initPuzzleView()
            }
        }
    }

    private fun initPuzzleView() {
        finishViewModel.player.observe(viewLifecycleOwner, Observer { code ->
            tv_code_puzzle.text = code
        })
        civ_puzzle_read.setOnClickListener {
            requireActivity()
                .getNavOwner()
                ?.addFragment(ReadFragment().apply {
                    arguments = Bundle().apply {
                        this.putInt(ReadFragment.READABLE_TYPE_KEY, ReadableType.PUZZLE.value)
                    }
                }, R.id.main_content)
        }
    }

    private fun initFinishView() {
        val target = DrawableImageViewTarget(civ_finish)
        Glide
            .with(this)
            .load(R.raw.finish_coin)
            .apply(RequestOptions
                .bitmapTransform(CropCircleWithBorderTransformation(10, resources.getColor(R.color.borderColorImageLight))))
            .into(target)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED, sticky = true)
    open fun onPuzzleFinishedEvent(event: PuzzleFinishedEvent) {
        val stickyEvent = EventBus.getDefault().removeStickyEvent(PuzzleFinishedEvent::class.java)
        stickyEvent?.let {
            if (event.playerCode.isNotEmpty()) {
                finishViewModel.checkPlayerCode(event.playerCode).invokeOnCompletion {
                    requireActivity().onBackPressed()
                }
                EventBus.getDefault().postSticky(RuleSignalEvent("puzzle"))
            }
        }
    }
}
