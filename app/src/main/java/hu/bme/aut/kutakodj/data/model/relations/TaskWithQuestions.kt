package hu.bme.aut.kutakodj.data.model.relations

import androidx.room.Embedded
import androidx.room.Relation
import hu.bme.aut.kutakodj.data.model.Question
import hu.bme.aut.kutakodj.data.model.Task

data class TaskWithQuestions(
    @Embedded val task: Task,
    @Relation(
        parentColumn = "taskId",
        entityColumn = "taskOwnerId"
    )
    val questions: List<Question> = emptyList()
)
