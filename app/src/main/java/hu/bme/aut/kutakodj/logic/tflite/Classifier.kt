package hu.bme.aut.kutakodj.logic.tflite

import android.app.Activity
import android.graphics.Bitmap
import android.os.SystemClock
import android.os.Trace
import android.util.Log
import hu.bme.aut.kutakodj.Constants
import hu.bme.aut.kutakodj.Constants.PREPROCESS_EDGES
import org.opencv.android.Utils
import org.opencv.core.Core.BORDER_DEFAULT
import org.opencv.core.Core.convertScaleAbs
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.Size
import org.opencv.imgproc.Imgproc
import org.opencv.imgproc.Imgproc.Laplacian
import org.tensorflow.lite.Interpreter
import org.tensorflow.lite.gpu.GpuDelegate
import org.tensorflow.lite.support.common.FileUtil
import org.tensorflow.lite.support.common.TensorOperator
import org.tensorflow.lite.support.common.TensorProcessor
import org.tensorflow.lite.support.image.ImageProcessor
import org.tensorflow.lite.support.image.TensorImage
import org.tensorflow.lite.support.image.ops.ResizeOp
import org.tensorflow.lite.support.image.ops.ResizeOp.ResizeMethod
import org.tensorflow.lite.support.image.ops.ResizeWithCropOrPadOp
import org.tensorflow.lite.support.image.ops.Rot90Op
import org.tensorflow.lite.support.label.TensorLabel
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer
import java.nio.MappedByteBuffer
import java.util.*
import kotlin.Comparator

abstract class Classifier protected constructor(
    activity: Activity,
    device: Device,
    numOfThreads: Int
) {
    enum class Device {
        CPU, GPU
    }

    /** The loaded TensorFlow Lite model.  */
    private var tfliteModel: MappedByteBuffer? = null

    /** Image size along the x axis.  */
    var imageSizeX = 0
    private set

    /** Image size along the y axis.  */
    var imageSizeY = 0
    private set

    /** Optional GPU delegate for acceleration.  */
    private var gpuDelegate: GpuDelegate? = null

    /** An instance of the driver class to run model inference with Tensorflow Lite.  */
    protected var tflite: Interpreter? = null

    /** Options for configuring the Interpreter.  */
    private val tfliteOptions: Interpreter.Options = Interpreter.Options()

    /** Labels corresponding to the output of the vision model.  */
    private var labels: MutableList<String>? = null

    /** Input image TensorBuffer.  */
    private var inputImageBuffer: TensorImage? = null

    /** Output probability TensorBuffer.  */
    private var outputProbabilityBuffer: TensorBuffer? = null

    /** Processer to apply post processing of the output probability.  */
    private var probabilityProcessor: TensorProcessor? = null

    companion object {
        fun create(activity: Activity, device: Device, numOfThreads: Int): Classifier =
            ClassifierFloatMobileNet(activity, device, numOfThreads)
    }

    init {
        val localTfliteModel = getModelPath()?.let { FileUtil.loadMappedFile(activity, it) }
        if (localTfliteModel == null) {
            Log.e("TFMODEL", "Model not found!")
        } else {
            tfliteModel = localTfliteModel
            when (device) {
                Device.GPU -> {
                    gpuDelegate = GpuDelegate()
                    tfliteOptions.addDelegate(gpuDelegate)
                }
                Device.CPU -> {
                }
            }
            tfliteOptions.setNumThreads(numOfThreads)

            val localTflite = Interpreter(localTfliteModel, tfliteOptions)
            tflite = localTflite

            // Loads labels out from the label file.
            labels = getLabelPath()?.let { FileUtil.loadLabels(activity, it) }

            // Reads type and shape of input and output tensors, respectively.
            val imageTensorIndex = 0
            val imageShape =
                localTflite.getInputTensor(imageTensorIndex).shape() // {1, height, width, 3}

            imageSizeY = imageShape[1]
            imageSizeX = imageShape[2]
            val imageDataType = localTflite.getInputTensor(imageTensorIndex).dataType()
            val probabilityTensorIndex = 0
            val probabilityShape =
                localTflite.getOutputTensor(probabilityTensorIndex).shape() // {1, NUM_CLASSES}

            val probabilityDataType = localTflite.getOutputTensor(probabilityTensorIndex).dataType()

            // Creates the input tensor.
            inputImageBuffer = TensorImage(imageDataType)

            // Creates the output tensor and its processor.
            outputProbabilityBuffer =
                TensorBuffer.createFixedSize(probabilityShape, probabilityDataType)

            // Creates the post processor for the output probability.
            probabilityProcessor =
                TensorProcessor.Builder().add(getPostprocessNormalizeOp()).build()

            Log.d("CLASSIFIER", "Created a Tensorflow Lite Image Classifier.")
        }
    }

    /** Runs inference and returns the classification results.  */
    open fun recognizeImage(
        bitmap: Bitmap?,
        sensorOrientation: Int
    ): List<Recognition?>? {
        // Logs this method so that it can be analyzed with systrace.
        Trace.beginSection("recognizeImage")
        Trace.beginSection("loadImage")
        val startTimeForLoadImage = SystemClock.uptimeMillis()
        inputImageBuffer = loadImage(bitmap, sensorOrientation)
        val endTimeForLoadImage = SystemClock.uptimeMillis()
        Trace.endSection()
        Log.v(
            "IMAGE_LOAD",
            "Timecost to load the image: ${(endTimeForLoadImage - startTimeForLoadImage)}"
        )

        // Runs the inference call.
        Trace.beginSection("runInference")
        val startTimeForReference = SystemClock.uptimeMillis()

        tflite?.run(inputImageBuffer?.buffer, outputProbabilityBuffer?.buffer?.rewind())
        val endTimeForReference = SystemClock.uptimeMillis()
        Trace.endSection()
        Log.v(
            "MODEL_INFERENCE",
            "Timecost to run model inference: ${(endTimeForReference - startTimeForReference)}"
        )

        // Gets the map of label and probability.
        val labeledProbability = if (labels != null && probabilityProcessor != null) {
            TensorLabel(labels!!, probabilityProcessor!!.process(outputProbabilityBuffer))
                .mapWithFloatValue
        } else {
            emptyMap()
        }

        Trace.endSection()

        // Gets top-k results.
        return getTopKProbability(labeledProbability)
    }

    /** Closes the interpreter and model to release resources.  */
    open fun close() {
        if (tflite != null) {
            tflite!!.close()
            tflite = null
        }

        if (gpuDelegate != null) {
            gpuDelegate!!.close()
            gpuDelegate = null
        }
        tfliteModel = null
    }

    /*
        Laplacian Edge detection with OpenCv
     */
    private fun preprocessBitmap(bitmap: Bitmap?): Bitmap {

        val ddepth = CvType.CV_16S
        val kernelSize = 3
        val scale = 1.0
        val delta = 0.0
        val outputMat = Mat()

        val rgba = Mat()
        Utils.bitmapToMat(bitmap, rgba)

        val noiseRemoved = Mat()
        Imgproc.GaussianBlur(rgba, noiseRemoved, Size(3.0, 3.0), 0.0, 0.0, BORDER_DEFAULT)
        val greyscale = Mat(rgba.size(), CvType.CV_8UC1)
        Imgproc.cvtColor(rgba, greyscale, Imgproc.COLOR_RGB2GRAY, 4)

        val laplacian = Mat()
        Laplacian(greyscale, laplacian, ddepth, kernelSize, scale, delta, BORDER_DEFAULT)
        convertScaleAbs(laplacian, outputMat)

        val resultBitmap = Bitmap.createBitmap(outputMat.cols(), outputMat.rows(), Bitmap.Config.ARGB_8888)
        Utils.matToBitmap(outputMat, resultBitmap)
        return resultBitmap
    }

    /** Loads input image, and applies preprocessing.  */
    private fun loadImage(bitmap: Bitmap?, sensorOrientation: Int): TensorImage? {
        // Loads bitmap into a TensorImage.
        if (bitmap == null) return null
        if (PREPROCESS_EDGES) inputImageBuffer?.load(preprocessBitmap(bitmap))
        else inputImageBuffer?.load(bitmap)

        // Creates processor for the TensorImage.
        val cropSize = bitmap.width.coerceAtMost(bitmap.height)
        val numRotation = sensorOrientation / 90
        // Define an ImageProcessor from TFLite Support Library to do preprocessing
        val imageProcessor = ImageProcessor.Builder()
            .add(ResizeWithCropOrPadOp(cropSize, cropSize))
            .add(ResizeOp(imageSizeX, imageSizeY, ResizeMethod.NEAREST_NEIGHBOR))
            .add(Rot90Op(numRotation))
            .add(getPreprocessNormalizeOp())
            .build()
        return imageProcessor.process(inputImageBuffer)
    }

    /** Gets the top-k results.  */
    private fun getTopKProbability(labelProb: Map<String, Float>): List<Recognition> {
        // Find the best classifications.
        val priorityQueue = PriorityQueue<Recognition>(
            Constants.MAX_RESULTS,
            Comparator<Recognition?> { lhs, rhs ->
                // Intentionally reversed to put high confidence at the head of the queue.
                if (rhs != null && lhs != null) {
                    return@Comparator rhs.confidence.compareTo(lhs.confidence)
                } else {
                    0
                }
            }
        )

        labelProb.forEach { (key, value) ->
            priorityQueue.add(Recognition("" + key, key, value, null))
        }

        val recognitions : MutableList<Recognition> = mutableListOf()
        val recognitionsSize = priorityQueue.size.coerceAtMost(Constants.MAX_RESULTS)
        for (i in 0 until recognitionsSize) {
            val item = priorityQueue.poll()
            item?.let {
                recognitions.add(it)
            }
        }
        return recognitions.toList()
    }

    /** Gets the name of the model file stored in Assets.  */
    protected abstract fun getModelPath(): String?

    /** Gets the name of the label file stored in Assets.  */
    protected abstract fun getLabelPath(): String?

    /** Gets the TensorOperator to nomalize the input image in preprocessing.  */
    protected abstract fun getPreprocessNormalizeOp(): TensorOperator?

    /**
     * Gets the TensorOperator to dequantize the output probability in post processing.
     *
     *
     * For quantized model, we need de-quantize the prediction with NormalizeOp (as they are all
     * essentially linear transformation). For float model, de-quantize is not required. But to
     * uniform the API, de-quantize is added to float model too. Mean and std are set to 0.0f and
     * 1.0f, respectively.
     */
    protected abstract fun getPostprocessNormalizeOp(): TensorOperator?
}