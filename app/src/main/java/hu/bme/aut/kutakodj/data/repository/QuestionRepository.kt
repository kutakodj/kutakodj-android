package hu.bme.aut.kutakodj.data.repository

import hu.bme.aut.kutakodj.data.dao.QuestionDao
import hu.bme.aut.kutakodj.data.model.Question
import hu.bme.aut.kutakodj.data.model.relations.QuestionWithAnswers

class QuestionRepository(private val dao: QuestionDao) {

    suspend fun getAll() = dao.getAll()

    suspend fun getById(id: Long): QuestionWithAnswers = dao.getById(id)

    suspend fun insertAll(vararg obj: Question) = dao.insertAll(*obj)

    suspend fun insert(obj: Question) = dao.insert(obj)

    suspend fun update(obj: Question) = dao.update(obj)

    suspend fun delete(obj: Question) = dao.delete(obj)
}
