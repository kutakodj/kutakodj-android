package hu.bme.aut.kutakodj.view.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.logic.viewmodels.QuestionViewModel
import hu.bme.aut.kutakodj.navigation.getNavOwner
import hu.bme.aut.kutakodj.util.AnswerClicked
import hu.bme.aut.kutakodj.view.adapter.AnswerListAdapter
import kotlinx.android.synthetic.main.fragment_question.*
import kotlinx.android.synthetic.main.fragment_question.view.*
import org.koin.androidx.viewmodel.ext.android.getSharedViewModel

class QuestionFragment : Fragment(R.layout.fragment_question), AnswerClicked {

    private lateinit var questionViewModel: QuestionViewModel
    private lateinit var adapter: AnswerListAdapter

    companion object {
        const val KEY_STUFF_ID = "stuff_id"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        questionViewModel = getSharedViewModel()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = AnswerListAdapter(requireActivity(), this, requireActivity().getNavOwner())
        recyclerview_answers.adapter = adapter

        questionViewModel.getNextQuestionFromPool(requireArguments().getLong(KEY_STUFF_ID))
        questionViewModel.currentQuestion.observe(viewLifecycleOwner, Observer { question ->
            if (question != null) {
                adapter.answerType = question.question.type.toInt()
                createLayoutManager(adapter.answerType)
                adapter.setItems(question.answers)
                view.tv_question.text = question.question.questionText
            }
        })

        questionViewModel.stationNumber.observe(viewLifecycleOwner, Observer { stationNumber ->
            view.tv_station_name.text = getString(R.string.question_station_name, stationNumber)
        })

    }

    private fun createLayoutManager(answerType: Int) {
        recyclerview_answers.layoutManager = when (answerType) {
            AnswerListAdapter.TYPE_PICTURE ->  LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            else -> LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        }
    }

    override fun onAnswerClicked(answerId: Long) {
        questionViewModel.pickedAnswerId = answerId
    }
}
