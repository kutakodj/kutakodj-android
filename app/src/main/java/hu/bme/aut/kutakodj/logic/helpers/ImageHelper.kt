package hu.bme.aut.kutakodj.logic.helpers

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import hu.bme.aut.kutakodj.Constants
import java.io.File
import java.io.FileOutputStream

object ImageHelper {
    /**
     * Utility method to compute the allocated size in bytes of a YUV420SP image of the given
     * dimensions.
     */
    fun getYUVByteSize(width: Int, height: Int): Int {
        // The luminance plane requires 1 byte per pixel.
        val ySize = width * height

        // The UV plane works on 2x2 blocks, so dimensions with odd size must be rounded up.
        // Each 2x2 block takes 2 bytes to encode, one each for U and V.
        val uvSize = ((width + 1) / 2) * ((height + 1) / 2) * 2
        return ySize + uvSize
    }

    /**
     * Saves a Bitmap object to disk for analysis.
     *
     * @param bitmap The bitmap to save.
     */
    fun saveBitmap(bitmap: Bitmap?, context: Context) {
        bitmap?.let {
            saveBitmap(bitmap, "preview.png", context)
        }
    }

    /**
     * Saves a Bitmap object to disk for analysis.
     *
     * @param bitmap The bitmap to save.
     * @param filename The location to save the bitmap to.
     */
    fun saveBitmap(bitmap: Bitmap, filename: String, context: Context) {
        val root = context.getExternalFilesDir(null)?.absolutePath + File.separator + "tensorflow"
        Log.i("BITMAP", "Saving ${bitmap.width}x${bitmap.height} bitmap to $root.")

        val myDir = File(root)
        if (!myDir.mkdirs()) Log.i("BITMAP","Make dir failed")

        val file = File(myDir, filename)
        if (file.exists()) {
            file.delete()
        }

        try {
            val out = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.PNG, 99, out)
            out.flush()
            out.close()
        } catch (e: Exception) {
            Log.e( "Bitmap exception!", e.stackTrace.toString())
        }
    }

    fun convertYUV420SPToARGB8888(
        input: ByteArray?,
        width: Int,
        height: Int,
        output: IntArray?
    ) {

        if (input != null && output != null) {
            val frameSize = width * height
            var j = 0
            var yp = 0
            while (j < height) {
                var uvp = frameSize + (j shr 1) * width
                var u = 0
                var v = 0
                var i = 0
                while (i < width) {
                    val y = 0xff and input[yp].toInt()
                    if (i and 1 == 0) {
                        v = 0xff and input[uvp++].toInt()
                        u = 0xff and input[uvp++].toInt()
                    }
                    output[yp] = YUV2RGB(y, u, v)
                    i++
                    yp++
                }
                j++
            }
        }
    }

    private fun YUV2RGB(inputY: Int, inputU: Int, inputV: Int): Int {
        // Adjust and check YUV values
        val y = if (inputY - 16 < 0) 0 else inputY - 16
        val u = inputU - 128
        val v = inputV - 128

        // This is the floating point equivalent. We do the conversion in integer
        // because some Android devices do not have floating point in hardware.
        // nR = (int)(1.164 * nY + 2.018 * nU);
        // nG = (int)(1.164 * nY - 0.813 * nV - 0.391 * nU);
        // nB = (int)(1.164 * nY + 1.596 * nV);
        val y1192 = 1192 * y
        var r = y1192 + 1634 * v
        var g = y1192 - 833 * v - 400 * u
        var b = y1192 + 2066 * u

        // Clipping RGB values to be inside boundaries [ 0 , kMaxChannelValue ]
        r = when {
            r > Constants.K_MAX_CHANNEL_VALUE -> Constants.K_MAX_CHANNEL_VALUE
            r < 0 -> 0
            else -> r
        }
        g = when {
            g > Constants.K_MAX_CHANNEL_VALUE -> Constants.K_MAX_CHANNEL_VALUE
            g < 0 -> 0
            else -> g
        }
        b = when {
            b > Constants.K_MAX_CHANNEL_VALUE -> Constants.K_MAX_CHANNEL_VALUE
            b < 0 -> 0
            else -> b
        }
        return -0x1000000 or ((r shl 6) and 0xff0000) or ((g shr 2) and 0xff00) or ((b shr 10) and 0xff)
    }

    fun convertYUV420ToARGB8888(
        yData: ByteArray?,
        uData: ByteArray?,
        vData: ByteArray?,
        width: Int,
        height: Int,
        yRowStride: Int,
        uvRowStride: Int,
        uvPixelStride: Int,
        out: IntArray?
    ) {
        if (yData != null && uData != null && vData != null && out != null) {
            var yp = 0
            for (j in 0 until height) {
                val pY = yRowStride * j
                val pUV = uvRowStride * (j shr 1)
                for (i in 0 until width) {
                    val uvOffset = pUV + (i shr 1) * uvPixelStride
                    out[yp++] = YUV2RGB(
                        0xff and yData[pY + i].toInt(),
                        0xff and uData[uvOffset].toInt(),
                        0xff and vData[uvOffset].toInt()
                    )
                }
            }
        }
    }
}