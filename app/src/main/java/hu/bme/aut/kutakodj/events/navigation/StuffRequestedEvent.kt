package hu.bme.aut.kutakodj.events.navigation

data class StuffRequestedEvent(val stuffId: Long)
