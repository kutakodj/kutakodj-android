package hu.bme.aut.kutakodj.data.dao
/*
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import hu.bme.aut.kutakodj.data.model.Signal
import hu.bme.aut.kutakodj.data.model.relations.SignalWithRules


@Dao
interface SignalDao : BaseDao<Signal> {
    @Transaction
    @Query("Select * from signal_table")
    fun getAll(): LiveData<List<SignalWithRules>>

    @Transaction
    @Query("Select * from signal_table where signalId = (:signalId)")
    fun getById(signalId: Long): LiveData<SignalWithRules>

    @Transaction
    @Query("Select * from signal_table where signalType = (:signalType)")
    fun getByType(signalType: String): LiveData<List<SignalWithRules>>
}


 */
