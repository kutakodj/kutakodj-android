package hu.bme.aut.kutakodj.util

import android.app.Activity
import android.util.Log
import android.view.Surface
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_LONG
import com.google.android.material.snackbar.Snackbar
import hu.bme.aut.kutakodj.R

fun Activity?.showToast(text: String) {
    this?.let { activity ->
        activity.runOnUiThread {
            val toast = Toast.makeText(activity, text, Toast.LENGTH_LONG)
            toast.view.apply {
                setBackgroundColor(resources.getColor(R.color.btnBgColorPrimary))
                this.findViewById<TextView>(android.R.id.message).apply {
                    setBackgroundColor(resources.getColor(R.color.btnBgColorPrimary))
                }
            }
            toast.show()
        }
    }
}

fun Activity?.showSnack(text: String) {
    this?.let { activity ->
        activity.runOnUiThread {
            Snackbar.make(window.decorView.findViewById(android.R.id.content), text, LENGTH_LONG)
        }
    }
}

fun Activity.getScreenOrientation(): Int {
    return when (windowManager.defaultDisplay.rotation) {
        Surface.ROTATION_270 -> 270
        Surface.ROTATION_180 -> 180
        Surface.ROTATION_90 -> 90
        else -> 0
    }
}

fun Exception.log(tag: String) {
    Log.e(tag, "${this.message} \n ${this.stackTraceToString()}")
}