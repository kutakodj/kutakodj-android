package hu.bme.aut.kutakodj.view.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.view.fragment.InfoFragment

class InfoPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    private val sites: MutableList<Fragment> = mutableListOf()

    init {
        val array = fragment.resources.getStringArray(R.array.info_descriptions)
        addInfoPage(array[0], R.raw.dimu_kutakodj_app_infopages_01_kut)
        addInfoPage(array[1], R.raw.dimu_kutakodj_app_infopages_02_mobil)
        addInfoPage(array[2], R.raw.dimu_kutakodj_app_infopages_03_tablet)
    }

    private fun addInfoPage(description: String, gif: Int) {
        sites.add(InfoFragment().apply {
            arguments = Bundle().apply {
                putString(InfoFragment.KEY_DESCRIPTION, description)
                putInt(InfoFragment.KEY_GIF, gif)
            }
        })
    }

    override fun getItemCount(): Int = sites.size

    override fun createFragment(position: Int): Fragment {
        return if (position < sites.size && position >= 0) sites[position]
        else throw IllegalArgumentException("No such page!")
    }

    override fun getItemId(position: Int): Long {
        return sites[position].hashCode().toLong()
    }
}
