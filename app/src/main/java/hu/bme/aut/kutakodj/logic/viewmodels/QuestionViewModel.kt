package hu.bme.aut.kutakodj.logic.viewmodels

import android.app.Application
import androidx.lifecycle.*
import hu.bme.aut.kutakodj.data.model.relations.QuestionWithAnswers
import hu.bme.aut.kutakodj.data.model.relations.RoomWithRewardsAndStuffs
import hu.bme.aut.kutakodj.data.model.relations.StuffAndTask
import hu.bme.aut.kutakodj.events.signals.RuleSignalEvent
import hu.bme.aut.kutakodj.logic.interactors.QuestionInteractor
import hu.bme.aut.kutakodj.logic.interactors.RoomInteractor
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus

class QuestionViewModel(
    application: Application,
    private val questionInteractor: QuestionInteractor,
    private val roomInteractor: RoomInteractor
) : AndroidViewModel(application) {

    var currentStuff: MutableLiveData<StuffAndTask> = MutableLiveData()
    var tryCount = 3
    var currentQuestion: MutableLiveData<QuestionWithAnswers> = MutableLiveData()
    var pickedAnswerId: Long? = null

    private var currentRoomOfStuff: MutableLiveData<RoomWithRewardsAndStuffs> = MutableLiveData()
    val stationNumber = MediatorLiveData<Int>()

    init {
        stationNumber.addSource(currentRoomOfStuff, Observer { room ->
            stationNumber.value =
                room.stuffs.indexOf(room.stuffs.find { it.stuffId == currentStuff.value!!.stuff.stuffId }) + 1
        })
    }

    fun getNextQuestionFromPool(stuffId: Long) = viewModelScope.launch {
        val stuff = roomInteractor.getStuffById(stuffId)
        currentStuff.postValue(stuff)
        currentRoomOfStuff.postValue(roomInteractor.getRoomById(stuff.stuff.containingRoomId))

        val nextQuestion = questionInteractor.getNextQuestionFromPool(stuff.task?.taskId ?: 1)
        if (nextQuestion != null) {
            // nextQuestion should be cast into a non nullable type, but removing !! causes error...
            currentQuestion.postValue(nextQuestion)
            tryCount--
        } else {
            questionInteractor.loadQuestionsOfTask(stuff.task?.taskId ?: 1)
        }
    }

    /*
    fun resetPool() {
        questionPool.clear()
    }

     */

    fun acquireStuff() = viewModelScope.launch {
        currentStuff.value?.stuff?.let { stuff ->
            roomInteractor.collectStuff(stuff)
        }
        EventBus.getDefault().postSticky(RuleSignalEvent("count"))
    }
}
