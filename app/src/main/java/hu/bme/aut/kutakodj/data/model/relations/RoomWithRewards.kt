package hu.bme.aut.kutakodj.data.model.relations

import androidx.room.Embedded
import androidx.room.Relation
import hu.bme.aut.kutakodj.data.model.Reward
import hu.bme.aut.kutakodj.data.model.Room

data class RoomWithRewards(
    @Embedded val room: Room,
    @Relation(
        parentColumn = "roomId",
        entityColumn = "roomOwnerId"
    )
    val rewards: List<Reward>
)
