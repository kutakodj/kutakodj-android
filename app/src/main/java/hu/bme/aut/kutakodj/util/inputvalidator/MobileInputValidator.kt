package hu.bme.aut.kutakodj.util.inputvalidator

import java.util.*

// format: KUTAKODJ:ROOMID:STUFFID
class MobileInputValidator :
    InputValidator {
    override fun validateInput(input: String?): String {
        input?.contains(":")?.let {
            val parts = input.split(":")
            if (isValid(parts)) return parts[2]
        }
        return ""
    }

    private fun isValid(parts: List<String>): Boolean {
        if (parts.size == 3) {
            if (parts[0].toUpperCase(Locale.ROOT) == "KUTAKODJ" && parts[1].toLongOrNull() != null && parts[2].toLongOrNull() != null) {
                return true
            }
        }
        return false
    }
}
