package hu.bme.aut.kutakodj.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.data.model.Answer
import hu.bme.aut.kutakodj.navigation.NavigationOwner
import hu.bme.aut.kutakodj.util.AnswerClicked
import hu.bme.aut.kutakodj.view.fragment.PickedAnswerFragment
import hu.bme.aut.kutakodj.view.viewholder.AnswerPictureViewHolder
import hu.bme.aut.kutakodj.view.viewholder.AnswerTextViewHolder

class AnswerListAdapter internal constructor(
    private val context: Context,
    private val listener: AnswerClicked,
    private val navigationOwner: NavigationOwner?
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var answers = emptyList<Answer>()
    var answerType = TYPE_TEXT

    companion object {
        const val TYPE_TEXT = 0
        const val TYPE_PICTURE = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_PICTURE -> {
                val itemView = inflater.inflate(R.layout.listitem_answer_picture, parent, false)
                AnswerPictureViewHolder(itemView)
            }
            else -> {
                val itemView = inflater.inflate(R.layout.listitem_answer_text, parent, false)
                AnswerTextViewHolder(itemView)
            }
        }
    }

    override fun getItemCount(): Int = answers.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = answers[position]
        when (getItemViewType(position)) {
            TYPE_PICTURE -> {
                (holder as AnswerPictureViewHolder).setPictureAnswerDetails(item, position, context)
            }
            else -> {
                (holder as AnswerTextViewHolder).setTextAnswerDetails(item, position)
            }
        }

        holder.itemView.setOnClickListener {
            listener.onAnswerClicked(item.answerId)
            navigationOwner?.replaceFragment(PickedAnswerFragment(), R.id.fragment_container_quiz)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (answerType) {
            TYPE_PICTURE -> TYPE_PICTURE
            else -> TYPE_TEXT
        }
    }

    fun setItems(items: List<Answer>) {
        answers = items
        notifyDataSetChanged()
    }
}
