package hu.bme.aut.kutakodj.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.events.navigation.StuffRequestedEvent
import hu.bme.aut.kutakodj.events.navigation.TaskRequestedEvent
import hu.bme.aut.kutakodj.events.signals.RuleSignalEvent
import hu.bme.aut.kutakodj.navigation.getNavOwner
import hu.bme.aut.kutakodj.util.switchStatusBarColor
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class WellBaseFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_well_base, container, false)
        activity?.switchStatusBarColor(R.color.bgColorPrimary)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val firstFragment = WellFragment()

        requireActivity()
            .getNavOwner()
            ?.addFragment(firstFragment, R.id.fragment_container_well)
    }

    private fun onInfoRequested(stuffId: Long) {
        requireActivity()
            .getNavOwner()
            ?.addFragment(StuffInfoFragment().apply {
                arguments = Bundle().apply {
                    this.putLong(StuffInfoFragment.KEY_STUFF_ID, stuffId)
                }
            }, R.id.fragment_container_well)
    }

    override fun onResume() {
        activity?.switchStatusBarColor(R.color.bgColorPrimary)
        EventBus.getDefault().register(this)
        super.onResume()
    }

    override fun onPause() {
        EventBus.getDefault().unregister(this)
        super.onPause()
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    open fun onTaskRequestedEvent(event: TaskRequestedEvent) {
        when (event.taskType) {
            // TODO switch quiz with collect
            "quiz" -> {
                onInfoRequested(event.stuffId)
                EventBus.getDefault().postSticky(
                    StuffRequestedEvent(
                        event.stuffId
                    )
                )
                EventBus.getDefault().postSticky(RuleSignalEvent("count"))
            }
            else -> {}
        }
    }
}
