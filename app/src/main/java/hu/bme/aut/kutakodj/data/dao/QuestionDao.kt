package hu.bme.aut.kutakodj.data.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import hu.bme.aut.kutakodj.data.model.Question
import hu.bme.aut.kutakodj.data.model.relations.QuestionWithAnswers

@Dao
interface QuestionDao : BaseDao<Question> {
    @Transaction
    @Query("Select * from question_table")
    suspend fun getAll(): List<QuestionWithAnswers>

    @Transaction
    @Query("Select * from question_table where questionId = (:questionId)")
    suspend fun getById(questionId: Long): QuestionWithAnswers

    @Transaction
    @Query("Delete from question_table")
    suspend fun deleteAll()
}
