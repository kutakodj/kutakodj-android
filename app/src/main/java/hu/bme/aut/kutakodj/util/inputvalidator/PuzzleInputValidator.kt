package hu.bme.aut.kutakodj.util.inputvalidator

import java.util.*

// Formátum: KUTAKODJ:PLAYERCODE:OK/NOTOK
class PuzzleInputValidator :
    InputValidator {

    override fun validateInput(input: String?): String {
        input?.contains(":")?.let {
            val parts = input.split(":")
            if (isValid(parts)) return parts[1]
        }
        return ""
    }

    private fun isValid(parts: List<String>): Boolean {
        if (parts.size == 3) {
            if (parts[0].toUpperCase(Locale.ROOT) == "KUTAKODJ" &&
                parts[1].toLongOrNull() != null &&
                (parts[2] == "OK")) {
                return true
            }
        }
        return false
    }
}
