package hu.bme.aut.kutakodj.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "achievement_table")
data class Achievement(
    @PrimaryKey(autoGenerate = true) val achievementId: Long,
    val name: String,
    val description: String,
    var acquired: Boolean
)
