package hu.bme.aut.kutakodj.data.repository

import androidx.lifecycle.LiveData
import hu.bme.aut.kutakodj.data.dao.AnswerDao
import hu.bme.aut.kutakodj.data.model.Answer

class AnswerRepository(private val dao: AnswerDao) {
    val allItems: LiveData<List<Answer>> = dao.getAll()

    fun getById(id: Long): LiveData<Answer> = dao.getById(id)

    fun getByQuestionId(id: Long): LiveData<List<Answer>> = dao.getByQuestionId(id)

    suspend fun insertAll(vararg obj: Answer) = dao.insertAll(*obj)

    suspend fun insert(obj: Answer) = dao.insert(obj)

    suspend fun update(obj: Answer) = dao.update(obj)

    suspend fun delete(obj: Answer) = dao.delete(obj)
}
