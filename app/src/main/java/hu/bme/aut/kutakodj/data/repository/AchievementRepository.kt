package hu.bme.aut.kutakodj.data.repository

import androidx.lifecycle.LiveData
import hu.bme.aut.kutakodj.data.dao.AchievementDao
import hu.bme.aut.kutakodj.data.model.Achievement
import hu.bme.aut.kutakodj.data.model.relations.AchievementWithRules

class AchievementRepository(private val dao: AchievementDao) {
    val allItems: LiveData<List<AchievementWithRules>> = dao.getAll()

    fun getById(id: Long): LiveData<AchievementWithRules> = dao.getById(id)

    suspend fun getAll(): List<AchievementWithRules> = dao.getAllSimple()

    suspend fun insertAll(vararg obj: Achievement) = dao.insertAll(*obj)

    suspend fun insert(obj: Achievement) = dao.insert(obj)

    suspend fun update(obj: Achievement) = dao.update(obj)

    suspend fun delete(obj: Achievement) = dao.delete(obj)
}
