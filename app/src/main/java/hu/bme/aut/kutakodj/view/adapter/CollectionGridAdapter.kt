package hu.bme.aut.kutakodj.view.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.github.florent37.viewanimator.ViewAnimator
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.data.model.Reward

class CollectionGridAdapter internal constructor(private val context: Context) :
    RecyclerView.Adapter<CollectionGridAdapter.RewardViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var rewards = emptyList<Reward>()
    private var visibilities = emptyList<Boolean>()
    private var animations: MutableList<Boolean> = mutableListOf()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CollectionGridAdapter.RewardViewHolder {
        val itemView = inflater.inflate(R.layout.listitem_reward, parent, false)
        return RewardViewHolder(itemView)
    }

    override fun getItemCount() = rewards.size

    override fun onBindViewHolder(holder: CollectionGridAdapter.RewardViewHolder, position: Int) {
        val reward = rewards[position]
        if (visibilities.isNullOrEmpty() || animations.isNullOrEmpty()) {
            holder.itemView.visibility = View.INVISIBLE
        } else if (visibilities[position] and !animations[position]) {
            holder.item_card.setImageResource(
                context.resources.getIdentifier(
                    "karpit_${reward.name}",
                    "drawable",
                    context.packageName
                )
            )
            holder.itemView.visibility = View.VISIBLE
            setAnimation(holder.itemView)
            animations[position] = true
        } else if (!visibilities[position]) {
            holder.itemView.visibility = View.INVISIBLE
        }
    }

    fun setItems(items: List<Reward>) {
        this.rewards = items
        notifyDataSetChanged()
    }

    fun setVisibilities(items: List<Boolean>) {
        this.visibilities = items
        if (animations.isNullOrEmpty()) {
            visibilities.forEach {
                Log.i("INIT", "$it")
                animations.add(false)
            }
            Log.i("ANIM", "$animations")
        }
        notifyDataSetChanged()
    }

    inner class RewardViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var item: Reward? = null
        val item_card = itemView.findViewById<ImageView>(R.id.item_card)
    }

    private fun setAnimation(viewToAnimate: View) {
        ViewAnimator
            /*.animate(viewToAnimate)
                .translationX(-2000F, 0F)
                .translationY(-2000F, 0F)
                .decelerate()
                .duration(1500)*/
            .animate(viewToAnimate)
            .duration(2000)
            .pulse()
            .repeatCount(1)
            .start()
    }
}
