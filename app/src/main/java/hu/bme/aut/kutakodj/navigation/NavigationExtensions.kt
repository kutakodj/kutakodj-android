package hu.bme.aut.kutakodj.navigation

import android.app.Activity

fun <T> MutableList<T>.push(item: T) = this.add(this.count(), item)
fun <T> MutableList<T>.pop(): T? = if (this.count() > 0) this.removeAt(this.count() - 1) else null
fun <T> MutableList<T>.peek(): T? = if (this.count() > 0) this[this.count() - 1] else null
fun <T> MutableList<T>.hasMore() = this.count() > 0

typealias Stack<T> = MutableList<T>

inline fun <T> mutableStackOf(): MutableList<T> = ArrayList()

fun Stack<Pair<Int, Stack<NavBundle>>>.addBackStack(navBundle: NavBundle, containerId: Int) {
    val stack = this.pop()
    if (stack == null) {
        val newStack = Pair(containerId, mutableStackOf<NavBundle>())
        newStack.second.push(navBundle)
        this.push(newStack)
    } else {
        if (stack.first == containerId) {
            val alreadyOnStack =
                stack.second.find { it.fragment.javaClass == navBundle.fragment.javaClass }
            if (alreadyOnStack != null) {
                val start = stack.second.indexOf(alreadyOnStack)
                for (i in start until stack.second.size) {
                    stack.second.pop()
                }
            }
            stack.second.push(navBundle)
            this.push(stack)
        } else {
            this.push(stack)
            val newStack = Pair(containerId, mutableStackOf<NavBundle>())
            newStack.second.push(navBundle)
            this.push(newStack)
        }
    }
}

fun Stack<Pair<Int, Stack<NavBundle>>>.popBackStackToTopOf(className: String): Pair<Int, NavBundle>? {
    return if (this.any { pair -> pair.second.any { it.fragment.javaClass.simpleName == className } }) {
        var popped: Pair<Int, NavBundle>?
        do {
            popped = this.popBackStack()
        } while (!(popped != null && popped.second.fragment.javaClass.simpleName == className))
        popped
    } else {
        null
    }
}

fun Stack<Pair<Int, Stack<NavBundle>>>.popBackStack(): Pair<Int, NavBundle>? {
    val stack = this.pop()
    var ret: Pair<Int, NavBundle>? = null
    if (stack != null) {
        if (stack.second.hasMore()) {
            val before = stack.second.pop()
            before?.let {
                ret = Pair(stack.first, it)
            }
        }
        if (stack.second.hasMore()) this.push(stack)
    }
    return ret
}

fun Activity?.getNavOwner(): NavigationOwner? {
    if (this is NavigationOwner) return this
    return null
}