package hu.bme.aut.kutakodj.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import hu.bme.aut.kutakodj.logic.enums.RuleType

@Entity(tableName = "rule_table")
data class Rule(
    @PrimaryKey(autoGenerate = true) val ruleId: Long,
    val achiOwnerId: Long,
    val ruleType: RuleType,
    val name: String,
    val ruleText: String,
    var done: Boolean,
    val countMax: Int,
    val countElement: String
)
