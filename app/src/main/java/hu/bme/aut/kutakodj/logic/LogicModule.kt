package hu.bme.aut.kutakodj.logic

import hu.bme.aut.kutakodj.logic.interactors.AchievementInteractor
import hu.bme.aut.kutakodj.logic.interactors.PlayerInteractor
import hu.bme.aut.kutakodj.logic.interactors.QuestionInteractor
import hu.bme.aut.kutakodj.logic.interactors.RoomInteractor
import hu.bme.aut.kutakodj.logic.viewmodels.*
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val logicModule = module {
    viewModel { CollectionViewModel(get(), get(), get()) }
    viewModel { FinishViewModel(get(), get()) }
    viewModel { QuestionViewModel(get(), get(), get()) }
    viewModel { QuizViewModel(get(), get()) }
    viewModel { WellViewModel(get(), get()) }

    single {
        AchievementInteractor(get(), get(), get(), get())
    }
    single {
        PlayerInteractor(get())
    }
    single {
        RoomInteractor(get(), get())
    }
    single { QuestionInteractor(get(), get()) }
}