package hu.bme.aut.kutakodj.data.model.relations

import androidx.room.Embedded
import androidx.room.Relation
import hu.bme.aut.kutakodj.data.model.Achievement
import hu.bme.aut.kutakodj.data.model.Rule

data class AchievementWithRules(
    @Embedded val achievement: Achievement,
    @Relation(
        parentColumn = "achievementId",
        entityColumn = "achiOwnerId"
    )
    val rules: List<Rule>
)
