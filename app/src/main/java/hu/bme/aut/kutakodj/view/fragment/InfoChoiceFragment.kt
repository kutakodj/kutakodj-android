package hu.bme.aut.kutakodj.view.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.navigation.getNavOwner
import kotlinx.android.synthetic.main.fragment_info_choice.*

class InfoChoiceFragment : Fragment(R.layout.fragment_info_choice) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        civ_gyik.setOnClickListener {
            requireActivity()
                .getNavOwner()
                ?.addFragment(InfoPanelHolderFragment(), R.id.main_content)
        }

        civ_about.setOnClickListener {
            requireActivity()
                .getNavOwner()
                ?.addFragment(AboutFragment(), R.id.main_content)
        }
    }
}
