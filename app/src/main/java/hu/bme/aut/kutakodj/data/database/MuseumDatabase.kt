package hu.bme.aut.kutakodj.data.database

/* ktlint-disable no-wildcard-imports */

import android.content.Context
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import hu.bme.aut.kutakodj.data.dao.*
import hu.bme.aut.kutakodj.data.model.*
import hu.bme.aut.kutakodj.logic.enums.Converters
import hu.bme.aut.kutakodj.logic.enums.toRuleType
import hu.bme.aut.kutakodj.util.toBoolean
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.InputStream

/* ktlint-enable no-wildcard-imports */

@Database(
    entities = arrayOf(
        Achievement::class,
        Answer::class,
        Player::class,
        Question::class,
        Reward::class,
        Room::class,
        Rule::class,
        Signal::class,
        Stuff::class,
        Task::class
    ),
    version = 1,
    exportSchema = true
)
@TypeConverters(Converters::class)
abstract class MuseumDatabase : RoomDatabase() {
    abstract fun AchievementDao(): AchievementDao
    abstract fun AnswerDao(): AnswerDao
    abstract fun PlayerDao(): PlayerDao
    abstract fun QuestionDao(): QuestionDao
    abstract fun RewardDao(): RewardDao
    abstract fun RoomDao(): RoomDao
    abstract fun RuleDao(): RuleDao
    // abstract fun SignalDao(): SignalDao
    abstract fun StuffDao(): StuffDao
    abstract fun TaskDao(): TaskDao

    companion object {
        @Volatile
        private var INSTANCE: MuseumDatabase? = null

        fun getDatabase(context: Context): MuseumDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) return tempInstance
            synchronized(this) {
                val instance = androidx.room.Room.databaseBuilder(
                    context.applicationContext,
                    MuseumDatabase::class.java,
                    "museum_database"
                )
                    .addCallback(MuseumDatabaseCallback())
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }

    private class MuseumDatabaseCallback : RoomDatabase.Callback() {

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            runBlocking {
                INSTANCE?.let { database ->
                    GlobalScope.launch {
                        readDbFromCSV(database)
                    }
                }
            }
        }

        suspend fun readDbFromCSV(database: MuseumDatabase) {
            val achievementDao = database.AchievementDao()
            val answerDao = database.AnswerDao()
            val playerDao = database.PlayerDao()
            val questionDao = database.QuestionDao()
            val rewardDao = database.RewardDao()
            val roomDao = database.RoomDao()
            val ruleDao = database.RuleDao()
            // val signalDao = database.SignalDao()
            val stuffDao = database.StuffDao()
            val taskDao = database.TaskDao()

            val achievements: MutableList<Achievement> = mutableListOf()
            val answers: MutableList<Answer> = mutableListOf()
            val players: MutableList<Player> = mutableListOf()
            val questions: MutableList<Question> = mutableListOf()
            val rewards: MutableList<Reward> = mutableListOf()
            val rooms: MutableList<Room> = mutableListOf()
            val rules: MutableList<Rule> = mutableListOf()
            val signals: MutableList<Signal> = mutableListOf()
            val stuffs: MutableList<Stuff> = mutableListOf()
            val tasks: MutableList<Task> = mutableListOf()

            val file: String = "res/raw/data.csv"
            val inputStream: InputStream = this.javaClass.classLoader!!.getResourceAsStream(file)

            csvReader {
                delimiter = ';'
                skipEmptyLine = true
            }.open(inputStream) {
                readAllAsSequence().forEach { row ->
                    when (row[0]) {
                        "achievement" -> {
                            achievements.add(Achievement(row[1].toLong(), row[2], row[3], row[4].toInt().toBoolean()))
                        }
                        "answer" -> {
                            answers.add(Answer(row[1].toLong(), row[2].toLong(), row[3]))
                        }
                        "player" -> {
                            players.add(Player(row[1].toLong(), row[2], row[3].toLong(), row[4], row[5].toInt().toBoolean()))
                        }
                        "question" -> {
                            questions.add(Question(row[1].toLong(), row[2], row[3].toLong(), row[4].toLong(), row[5].toLong()))
                        }
                        "reward" -> {
                            rewards.add(Reward(row[1].toLong(), row[2].toLong(), row[3]))
                        }
                        "room" -> {
                            rooms.add(Room(row[1].toLong(), row[2].toLong(), row[3], row[4].toInt().toBoolean()))
                        }
                        "rule" -> {
                            rules.add(Rule(row[1].toLong(), row[2].toLong(), row[3].toRuleType(), row[4], row[5], row[6].toInt().toBoolean(), row[7].toInt(), row[8]))
                        }
                        "signal" -> {
                            signals.add(Signal(row[1].toLong(), row[2]))
                        }
                        "stuff" -> {
                            stuffs.add(Stuff(row[1].toLong(), row[2], row[3].toInt().toBoolean(), row[4].toLong(), row[5], row[6]))
                        }
                        "task" -> {
                            tasks.add(Task(row[1].toLong(), row[2].toLong(), row[3]))
                        }
                        else -> {}
                    }
                }
            }

            achievementDao.insertAll(*achievements.toTypedArray())
            answerDao.insertAll(*answers.toTypedArray())
            playerDao.insertAll(*players.toTypedArray())
            questionDao.insertAll(*questions.toTypedArray())
            rewardDao.insertAll(*rewards.toTypedArray())
            roomDao.insertAll(*rooms.toTypedArray())
            ruleDao.insertAll(*rules.toTypedArray())
            // signalDao.insertAll(*signals.toTypedArray())
            stuffDao.insertAll(*stuffs.toTypedArray())
            taskDao.insertAll(*tasks.toTypedArray())
            questionDao.insertAll(*questions.toTypedArray())
            answerDao.insertAll(*answers.toTypedArray())
        }
    }
}
