package hu.bme.aut.kutakodj.util

interface AnswerClicked {
    fun onAnswerClicked(answerId: Long)
}
