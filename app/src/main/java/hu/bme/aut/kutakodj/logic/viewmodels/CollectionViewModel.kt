package hu.bme.aut.kutakodj.logic.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import hu.bme.aut.kutakodj.data.model.Reward
import hu.bme.aut.kutakodj.data.model.relations.RoomWithRewardsAndStuffs
import hu.bme.aut.kutakodj.data.repository.RewardRepository
import hu.bme.aut.kutakodj.data.repository.RoomRepository
import hu.bme.aut.kutakodj.util.combineWith

class CollectionViewModel(
    application: Application,
    private val rewardRepository: RewardRepository,
    private val roomRepository: RoomRepository
) : AndroidViewModel(application) {

    val rewards: LiveData<List<Reward>>

    private val maxNum = MediatorLiveData<Int>()
    val currentNum = MediatorLiveData<Int>()
    val points: LiveData<String>

    val visibleRewards = MediatorLiveData<List<Boolean>>()

    val allRooms: LiveData<List<RoomWithRewardsAndStuffs>>

    init {

        rewards = rewardRepository.allItems
        allRooms = roomRepository.allRooms

        maxNum.addSource(allRooms, Observer { rooms ->
            var sum = 0
            rooms.forEach {
                sum += it.stuffs.size
            }
            maxNum.value = sum
        })

        currentNum.addSource(allRooms, Observer { rooms ->
            var current = 0
            rooms.forEach { room ->
                current += room.stuffs.count { it.isCollected }
            }
            currentNum.value = current
        })

        points = currentNum.combineWith(maxNum) { current, max ->
            "$current/$max"
        }

        visibleRewards.addSource(allRooms, Observer { rooms ->
            var visibleList = mutableListOf<Boolean>()
            rooms.forEach { room ->
                room.stuffs.forEach {
                    if (it.isCollected) visibleList.add(true)
                    else visibleList.add(false)
                }
            }
            visibleRewards.value = visibleList
        })
    }
}
