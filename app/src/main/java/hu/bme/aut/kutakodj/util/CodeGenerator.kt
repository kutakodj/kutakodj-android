package hu.bme.aut.kutakodj.util

import kotlin.random.Random

class CodeGenerator {

    fun generateCodeForPlayer(): String {
        var center = generateEvenNumber()
        var sides = generatePuzzleNumber()
        if (center.length < 2) center = center.padStart(2, '0')
        if (sides.length < 4) sides = sides.padStart(4, '0')
        var code = "${sides.substring(0..1)}$center${sides.substring(2..3)}"
        return code
    }

    private fun generateEvenNumber(): String {
        val randomGenerator = Random
        return (randomGenerator.nextInt(0, 49) * 2).toString()
    }

    private fun generatePuzzleNumber(): String {
        val randomGenerator = Random
        return (randomGenerator.nextInt(0, 1428) * 7 + 2).toString()
    }
}
