package hu.bme.aut.kutakodj.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "signal_table")
data class Signal(
    @PrimaryKey(autoGenerate = true) val signalId: Long,
    val signalType: String
)
