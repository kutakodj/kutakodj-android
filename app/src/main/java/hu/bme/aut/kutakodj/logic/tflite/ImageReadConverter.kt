package hu.bme.aut.kutakodj.logic.tflite

object ImageReadConverter {
    fun convertLabelToId(label: String): String {
        return when (label) {
            "csizma" -> "009"
            "erme" -> "010"
            "csont" -> "011"
            "vaza" -> "012"
            else -> ""
        }
    }
}