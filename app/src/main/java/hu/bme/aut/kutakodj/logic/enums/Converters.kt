package hu.bme.aut.kutakodj.logic.enums

import androidx.room.TypeConverter

class Converters {
    @TypeConverter
    fun fromRuleType(value: RuleType) = value.value

    @TypeConverter
    fun toRuleType(value: String) = RuleType.values().find { it.value == value } ?: RuleType.NONE

}