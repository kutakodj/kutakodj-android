package hu.bme.aut.kutakodj.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.data.model.relations.StuffAndTask
import hu.bme.aut.kutakodj.navigation.NavigationOwner
import hu.bme.aut.kutakodj.view.viewholder.WellViewHolder

class WellListAdapter internal constructor(private val context: Context) : RecyclerView.Adapter<WellViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var stuffs = emptyList<StuffAndTask>()
    var navigationOwner: NavigationOwner? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WellViewHolder {
        val itemView = inflater.inflate(R.layout.listitem_quiz, parent, false)
        return WellViewHolder(itemView, navigationOwner)
    }

    override fun getItemCount(): Int = stuffs.size

    override fun onBindViewHolder(holder: WellViewHolder, position: Int) {
        val item = stuffs[position]
        if (item.stuff.isCollected) {
            holder.tv_well_item_name.text = item.stuff.name
            holder.civ_listitem_well.setImageResource(context.resources.getIdentifier(item.stuff.picture, "drawable", context.packageName))
        } else {
            holder.tv_well_item_name.text = "????"
            holder.civ_listitem_well.setImageResource(context.resources.getIdentifier("question_mark", "drawable", context.packageName))
        }
        holder.item = item
    }

    fun setItems(items: List<StuffAndTask>) {
        stuffs = items
        notifyDataSetChanged()
    }
}
