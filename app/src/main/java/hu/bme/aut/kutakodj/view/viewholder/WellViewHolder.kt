package hu.bme.aut.kutakodj.view.viewholder

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textview.MaterialTextView
import com.mikhaellopez.circularimageview.CircularImageView
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.data.model.relations.StuffAndTask
import hu.bme.aut.kutakodj.navigation.NavigationOwner
import hu.bme.aut.kutakodj.view.fragment.StuffInfoFragment

class WellViewHolder(itemView: View, navigationOwner: NavigationOwner?) : RecyclerView.ViewHolder(itemView) {

    val civ_listitem_well: CircularImageView = itemView.findViewById(R.id.civ_listitem_well)
    val tv_well_item_name: MaterialTextView = itemView.findViewById(R.id.tv_well_item_name)
    var item: StuffAndTask? = null

    init {
        navigationOwner?.let { navOwner ->
            itemView.setOnClickListener {
                navOwner
                    .addFragment(StuffInfoFragment().apply {
                        arguments = Bundle().apply {
                            item?.let {
                                this.putLong(StuffInfoFragment.KEY_STUFF_ID, it.stuff.stuffId)
                            }
                        }
                    }, R.id.fragment_container_well)
            }
        }
    }
}
