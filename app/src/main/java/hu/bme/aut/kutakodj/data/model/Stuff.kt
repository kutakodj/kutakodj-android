package hu.bme.aut.kutakodj.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "stuff_table")
data class Stuff(
    @PrimaryKey(autoGenerate = true) val stuffId: Long,
    val name: String,
    var isCollected: Boolean,
    val containingRoomId: Long,
    val description: String,
    val picture: String
)
