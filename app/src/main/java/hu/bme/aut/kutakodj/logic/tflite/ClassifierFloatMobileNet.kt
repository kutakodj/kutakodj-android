package hu.bme.aut.kutakodj.logic.tflite

import android.app.Activity
import hu.bme.aut.kutakodj.Constants.IMAGE_MEAN
import hu.bme.aut.kutakodj.Constants.IMAGE_STD
import hu.bme.aut.kutakodj.Constants.PROBABILITY_MEAN
import hu.bme.aut.kutakodj.Constants.PROBABILITY_STD
import org.tensorflow.lite.support.common.TensorOperator
import org.tensorflow.lite.support.common.ops.NormalizeOp

class ClassifierFloatMobileNet(
    activity: Activity,
    device: Device,
    numOfThreads: Int
) : Classifier(activity, device, numOfThreads) {

    override fun getModelPath(): String? = "model.tflite"

    override fun getLabelPath(): String? = "labels.txt"

    override fun getPreprocessNormalizeOp(): TensorOperator? = NormalizeOp(IMAGE_MEAN, IMAGE_STD)

    override fun getPostprocessNormalizeOp(): TensorOperator? = NormalizeOp(PROBABILITY_MEAN, PROBABILITY_STD)

}