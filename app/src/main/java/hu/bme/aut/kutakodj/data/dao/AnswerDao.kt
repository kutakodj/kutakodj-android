package hu.bme.aut.kutakodj.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import hu.bme.aut.kutakodj.data.model.Answer

@Dao
interface AnswerDao : BaseDao<Answer> {
    @Transaction
    @Query("Select * from answer_table")
    fun getAll(): LiveData<List<Answer>>

    @Transaction
    @Query("Select * from answer_table where answerId = (:answerId)")
    fun getById(answerId: Long): LiveData<Answer>

    @Transaction
    @Query("Select * from answer_table where questionOwnerId = (:questionId)")
    fun getByQuestionId(questionId: Long): LiveData<List<Answer>>

    @Transaction
    @Query("Delete from answer_table")
    suspend fun deleteAll()
}
