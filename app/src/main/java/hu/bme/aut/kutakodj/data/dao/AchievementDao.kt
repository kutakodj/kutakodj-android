package hu.bme.aut.kutakodj.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import hu.bme.aut.kutakodj.data.model.Achievement
import hu.bme.aut.kutakodj.data.model.relations.AchievementWithRules

@Dao
interface AchievementDao : BaseDao<Achievement> {
    @Transaction
    @Query("Select * from achievement_table")
    fun getAll(): LiveData<List<AchievementWithRules>>

    @Transaction
    @Query("Select * from achievement_table")
    suspend fun getAllSimple(): List<AchievementWithRules>

    @Transaction
    @Query("Select * from achievement_table where achievementId = (:achievementId)")
    fun getById(achievementId: Long): LiveData<AchievementWithRules>
}
