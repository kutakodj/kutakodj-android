package hu.bme.aut.kutakodj.view.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textview.MaterialTextView
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.data.model.Answer
import hu.bme.aut.kutakodj.util.getCharFromPosition

class AnswerTextViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val tv_answerCount: MaterialTextView = itemView.findViewById(R.id.tv_answer_count)
    val tv_answerDescription: MaterialTextView = itemView.findViewById(R.id.tv_answer_description)

    var item: Answer? = null

    fun setTextAnswerDetails(item: Answer, position: Int) {
        tv_answerCount.text = getCharFromPosition(position)
        tv_answerDescription.text = item.answerText
        this.item = item
    }
}
