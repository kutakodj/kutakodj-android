package hu.bme.aut.kutakodj.view.fragment

import android.Manifest
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.fragment.app.Fragment
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.livinglifetechway.quickpermissions_kotlin.util.QuickPermissionsOptions
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.logic.enums.ReadableType
import hu.bme.aut.kutakodj.logic.enums.ReaderType
import hu.bme.aut.kutakodj.logic.enums.ValidatorType
import hu.bme.aut.kutakodj.logic.enums.toReadableType
import hu.bme.aut.kutakodj.util.showToast
import hu.bme.aut.kutakodj.util.switchStatusBarColor
import hu.bme.aut.kutakodj.view.activity.BaseActivity
import hu.bme.aut.kutakodj.view.activity.MainActivity
import kotlinx.android.synthetic.main.fragment_read.*

class ReadFragment : Fragment(R.layout.fragment_read) {

    companion object {
        const val READABLE_TYPE_KEY = "readableType"
    }

    private object Animator {
        const val NFC = 0
        const val CAMERA = 1
    }

    private val quickPermissionsOption = QuickPermissionsOptions(
        permanentDeniedMethod = { req -> requireActivity().showToast(getString(R.string.permission_error)) },
        permissionsDeniedMethod = {req -> requireActivity().showToast(getString(R.string.permission_error)) }
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().switchStatusBarColor(R.color.bgColorPrimaryDark)
        initAnimator()
        initDescriptionText()
    }

    override fun onResume() {
        super.onResume()
        arguments?.getInt(READABLE_TYPE_KEY)?.toReadableType()?.let {
            (requireActivity() as MainActivity).currentReadableType = it
            when(it) {
                ReadableType.PUZZLE -> (requireActivity() as MainActivity).activateReading(ReaderType.QR, ValidatorType.PUZZLE)
                ReadableType.QUIZ -> (requireActivity() as MainActivity).activateReading(ReaderType.QR)
                ReadableType.WELL -> (requireActivity() as MainActivity).activateReading(ReaderType.IMAGE)
            }
        }
    }

    override fun onPause() {
        (requireActivity() as MainActivity).deactivateReading()
        (requireActivity() as MainActivity).currentReadableType = null
        super.onPause()
    }

    private fun initAnimator() {
        arguments?.getInt(READABLE_TYPE_KEY)?.toReadableType()?.let {
            view_animator_read.displayedChild = when(it) {
                ReadableType.PUZZLE -> Animator.CAMERA
                ReadableType.QUIZ, ReadableType.WELL -> {
                    when ((requireActivity() as MainActivity).getAvailableReadingType()) {
                        ReaderType.NFC -> Animator.NFC
                        else -> Animator.CAMERA
                    }
                }
            }
            if (view_animator_read.displayedChild == Animator.CAMERA) {
                initCameraFragment()
            }
        }
    }

    private fun initDescriptionText() {
        arguments?.getInt(READABLE_TYPE_KEY)?.toReadableType()?.let {
            tv_action_description.text = getDescriptionByReaderType(it)
        }
    }

    private fun initCameraFragment() {
        // runWithPermission must be called here because camera access should be asked
        // in the first place one wishes to use a layout with the camera as well
        // also the handler is needed because fragmentTransaction could still be underway
        // and that causes an exception...
        Handler().post {
            runWithPermissions(Manifest.permission.CAMERA, options = quickPermissionsOption) {
                val fragment = (requireActivity() as BaseActivity).getCameraFragment()
                fragment?.let {
                    childFragmentManager
                        .beginTransaction()
                        .add(R.id.camera_placeholder, it)
                        .commit()
                }
            }
        }
    }

    private fun getDescriptionByReaderType(readableType: ReadableType): String {
        val availableReadingType = (requireActivity() as MainActivity).getAvailableReadingType()

        return when (readableType) {
            ReadableType.WELL -> {
                when(availableReadingType) {
                    ReaderType.NFC -> resources.getString(R.string.read_description_NFC_well)
                    ReaderType.QR -> resources.getString(R.string.read_description_QR_well)
                    ReaderType.IMAGE -> getString(R.string.read_description_image_well)
                    else -> {""}
                }
            }
            ReadableType.QUIZ -> {
                when(availableReadingType) {
                    ReaderType.NFC -> resources.getString(R.string.read_description_NFC_quiz)
                    ReaderType.QR -> resources.getString(R.string.read_description_QR_quiz)
                    ReaderType.IMAGE -> getString(R.string.read_description_image_quiz)
                    else -> {""}
                }
            }
            ReadableType.PUZZLE -> resources.getString(R.string.read_description_QR_well)
        }
    }
}
