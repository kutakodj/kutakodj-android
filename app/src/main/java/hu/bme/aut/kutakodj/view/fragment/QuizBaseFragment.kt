package hu.bme.aut.kutakodj.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import hu.bme.aut.kutakodj.R
import hu.bme.aut.kutakodj.events.navigation.TaskRequestedEvent
import hu.bme.aut.kutakodj.navigation.getNavOwner
import hu.bme.aut.kutakodj.util.switchStatusBarColor
import hu.bme.aut.kutakodj.view.fragment.QuestionFragment.Companion.KEY_STUFF_ID
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class QuizBaseFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_quiz_base, container, false)
        activity?.switchStatusBarColor(R.color.bgColorPrimaryDark)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val firstFragment = QuizFragment()

        requireActivity()
            .getNavOwner()
            ?.addFragment(firstFragment, R.id.fragment_container_quiz)
    }

    private fun onQuestionRequested(stuffId: Long) {
        requireActivity()
            .getNavOwner()
            ?.replaceFragment(QuestionFragment().apply {
                arguments = Bundle().apply {
                    putLong(KEY_STUFF_ID, stuffId)
                }
            }, R.id.fragment_container_quiz)
    }

    override fun onResume() {
        activity?.switchStatusBarColor(R.color.bgColorPrimaryDark)
        EventBus.getDefault().register(this)
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    open fun onTaskRequestedEvent(event: TaskRequestedEvent) {
        when (event.taskType) {
            // TODO swithc collect with quiz
            "collect" -> {
                onQuestionRequested(event.stuffId)
            }
            else -> {}
        }
    }
}
