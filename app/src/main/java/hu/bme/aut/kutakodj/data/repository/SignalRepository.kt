package hu.bme.aut.kutakodj.data.repository

/*
import androidx.lifecycle.LiveData
import hu.bme.aut.kutakodj.data.dao.SignalDao
import hu.bme.aut.kutakodj.data.model.Signal
import hu.bme.aut.kutakodj.data.model.relations.SignalWithRules

class SignalRepository(private val dao: SignalDao) {
    val allItems: LiveData<List<SignalWithRules>> = dao.getAll()

    fun getById(id: Long): LiveData<SignalWithRules> = dao.getById(id)

    fun getByType(type: String): LiveData<List<SignalWithRules>> = dao.getByType(type)

    suspend fun insertAll(vararg obj: Signal) = dao.insertAll(*obj)

    suspend fun insert(obj: Signal) = dao.insert(obj)

    suspend fun update(obj: Signal) = dao.update(obj)

    suspend fun delete(obj: Signal) = dao.delete(obj)
}

 */
