package hu.bme.aut.kutakodj.data.model.relations

import androidx.room.Embedded
import androidx.room.Relation
import hu.bme.aut.kutakodj.data.model.Room
import hu.bme.aut.kutakodj.data.model.Stuff

data class RoomWithStuffs(
    @Embedded val room: Room,
    @Relation(
        parentColumn = "roomId",
        entityColumn = "containingRoomId"
    )
    val stuffs: List<Stuff>
)
